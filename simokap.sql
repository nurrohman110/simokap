-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 13 Apr 2021 pada 04.40
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `simokap`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_02_11_120238_create_tabel_sisa_cuti', 1),
(5, '2021_02_11_120259_create_tabel_user_level', 1),
(6, '2021_02_11_120323_create_tabel_activity_harian', 1),
(7, '2021_02_11_120342_create_tabel_pangkat_master', 1),
(8, '2021_02_11_120401_create_tabel_jabatan_pegawai', 1),
(9, '2021_02_11_120429_create_tabel_jenis_cuti', 1),
(10, '2021_02_11_141652_create_tabel_agama', 1),
(11, '2021_02_11_152201_create_tabel_detail_activity', 1),
(12, '2021_02_12_094018_create_tabel_pengajuan_cuti', 1),
(13, '2021_02_12_115535_create_tabel_module', 1),
(14, '2021_02_12_115558_create_tabel_modul_akses', 1),
(15, '2021_02_17_184455_create_tabel_jenis_pegawai', 2),
(16, '2021_03_15_215002_create_tabel_schedule_naik_gaji', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_activity_harian`
--

CREATE TABLE `tabel_activity_harian` (
  `id` int(11) NOT NULL,
  `kode_user` varchar(191) NOT NULL,
  `kode_activity` varchar(191) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `description` text DEFAULT NULL,
  `parent_kode_activity` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tabel_activity_harian`
--

INSERT INTO `tabel_activity_harian` (`id`, `kode_user`, `kode_activity`, `tanggal`, `description`, `parent_kode_activity`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'KD1', 'AH1', '2021-03-26', NULL, NULL, '2021-03-27 14:33:49', NULL, NULL),
(2, 'KD1', NULL, '2021-03-26', 'afkladlgadg', 'AH1', '2021-03-27 14:33:49', NULL, NULL),
(3, 'KD1', NULL, '2021-03-26', NULL, 'AH1', '2021-03-27 14:33:49', NULL, NULL),
(4, 'KD1', 'AH2', '2021-03-26', NULL, NULL, '2021-03-27 14:41:20', NULL, NULL),
(5, 'KD1', NULL, '2021-03-26', 'dkalsjflkadf', 'AH2', '2021-03-27 14:41:20', NULL, NULL),
(6, 'KD1', 'AH3', '2021-03-26', NULL, NULL, '2021-03-27 14:41:32', NULL, NULL),
(7, 'KD1', NULL, '2021-03-26', 'asfkaljf', 'AH3', '2021-03-27 14:41:32', NULL, NULL),
(8, 'KD1', NULL, '2021-03-26', 'asflkajdgadg', 'AH3', '2021-03-27 14:41:32', NULL, NULL),
(9, 'KD3', 'AH4', '2021-03-31', NULL, NULL, '2021-04-01 08:21:48', NULL, NULL),
(10, 'KD3', NULL, '2021-03-31', 'testing', 'AH4', '2021-04-01 08:21:48', NULL, NULL),
(11, 'KD3', NULL, '2021-03-31', 'testing 2', 'AH4', '2021-04-01 08:21:48', NULL, NULL),
(12, 'KD4', 'AH5', '2021-04-01', NULL, NULL, '2021-04-01 08:23:48', NULL, NULL),
(13, 'KD4', NULL, '2021-04-01', 'testing', 'AH5', '2021-04-01 08:23:48', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_agama`
--

CREATE TABLE `tabel_agama` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_agama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tabel_agama`
--

INSERT INTO `tabel_agama` (`id`, `nama_agama`, `created_at`, `updated_at`) VALUES
(1, 'Islam', '2021-02-13 02:33:37', NULL),
(2, 'Protestan', '2021-02-13 02:33:37', NULL),
(3, 'Katolik', '2021-02-13 02:33:37', NULL),
(4, 'Hindu', '2021-02-13 02:33:37', NULL),
(5, 'Buddha', '2021-02-13 02:33:37', NULL),
(6, 'Khonghucu', '2021-02-13 02:33:37', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_detail_activity`
--

CREATE TABLE `tabel_detail_activity` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode_activity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_jenis_cuti`
--

CREATE TABLE `tabel_jenis_cuti` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_jenis_cuti` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_jenis_pegawai`
--

CREATE TABLE `tabel_jenis_pegawai` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_jenis` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tabel_jenis_pegawai`
--

INSERT INTO `tabel_jenis_pegawai` (`id`, `nama_jenis`, `created_at`, `updated_at`) VALUES
(1, 'PNS', '2021-02-17 11:49:35', NULL),
(2, 'CPNS', '2021-02-17 11:49:35', NULL),
(3, 'PPNPN', '2021-02-17 11:49:35', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_modul`
--

CREATE TABLE `tabel_modul` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_modul` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tabel_modul`
--

INSERT INTO `tabel_modul` (`id`, `nama_modul`, `url`, `icon`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Dashboard', 'home', 'fa fa-dashboard', '2021-02-13 02:33:37', NULL, NULL),
(2, 'Aktifitas Harian', 'activity-harian', 'fa fa-calendar-o', '2021-02-13 02:33:37', NULL, NULL),
(3, 'Pengajuan Cuti', 'pengajuan-cuti', 'fa fa-envelope', '2021-02-13 02:33:37', NULL, NULL),
(4, 'Riwayat Kepangkatan', 'riwayat-pangkat', 'fa fa-bookmark', '2021-02-13 02:33:37', NULL, NULL),
(5, 'Pangkat Master', 'pangkat', 'fa fa-cubes', '2021-02-13 02:33:37', NULL, NULL),
(6, 'Pegawai', 'pegawai', 'fa fa-users', '2021-02-13 02:33:37', NULL, NULL),
(7, 'User Level', 'user-level', 'fa fa-child', '2021-02-13 02:33:37', NULL, NULL),
(8, 'Type Cuti', 'type-cuti', 'fa fa-tasks', '2021-02-13 02:33:37', NULL, '2021-02-22 11:59:24'),
(9, 'Agama', 'agama', 'fa fa-shield', '2021-02-13 02:33:37', NULL, NULL),
(10, 'Modul Akses', 'modul', 'fa fa-cog', '2021-02-13 02:33:37', NULL, NULL),
(11, 'Management Access', 'management-access', 'fa fa-unlock-alt', '2021-02-13 02:33:37', NULL, NULL),
(12, 'Sisa Cuti', 'sisa_cuti', 'fa fa-database', '2021-02-14 01:25:55', NULL, NULL),
(15, 'Kenaikan Gaji', 'kenaikan-gaji', 'fa fa-list-ul', '2021-02-14 01:25:55', NULL, '2021-03-16 14:46:32'),
(16, 'Jadwal Kenaikan Gaji', 'kenaikan-gaji', 'fa fa-bar-chart-o', '2021-03-16 14:35:19', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_modul_akses`
--

CREATE TABLE `tabel_modul_akses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_level` int(11) NOT NULL,
  `id_modul` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tabel_modul_akses`
--

INSERT INTO `tabel_modul_akses` (`id`, `id_level`, `id_modul`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '2021-02-13 02:33:37', NULL, NULL),
(2, 1, 2, '2021-02-13 02:33:37', NULL, NULL),
(3, 1, 3, '2021-02-13 02:33:37', NULL, NULL),
(4, 1, 4, '2021-02-13 02:33:37', NULL, NULL),
(5, 1, 5, '2021-02-13 02:33:37', NULL, NULL),
(6, 1, 6, '2021-02-13 02:33:37', NULL, NULL),
(7, 1, 7, '2021-02-13 02:33:37', NULL, NULL),
(8, 1, 8, '2021-02-13 02:33:37', NULL, '2021-02-22 11:59:35'),
(9, 1, 9, '2021-02-13 02:33:37', NULL, NULL),
(10, 1, 10, '2021-02-13 02:33:37', NULL, NULL),
(11, 1, 11, '2021-02-13 02:33:37', NULL, NULL),
(12, 2, 1, '2021-02-14 02:25:24', '2021-02-14 03:11:50', '2021-02-14 07:41:18'),
(13, 2, 3, '2021-02-14 07:36:17', '2021-02-14 03:11:14', '2021-02-14 07:42:02'),
(14, 1, 12, '2021-02-14 07:36:17', '2021-02-14 03:11:14', '2021-02-14 07:42:02'),
(15, 2, 1, '2021-03-04 11:23:13', NULL, NULL),
(16, 2, 2, '2021-03-04 11:23:41', NULL, NULL),
(17, 2, 3, '2021-03-04 11:23:49', NULL, NULL),
(18, 2, 4, '2021-03-04 11:24:05', NULL, NULL),
(19, 3, 1, '2021-03-04 11:24:32', NULL, NULL),
(20, 2, 5, '2021-03-04 11:24:40', NULL, NULL),
(21, 2, 6, '2021-03-04 11:24:48', NULL, NULL),
(22, 3, 2, '2021-03-04 11:25:06', NULL, NULL),
(23, 3, 3, '2021-03-04 11:25:16', NULL, NULL),
(24, 3, 4, '2021-03-04 11:25:23', NULL, NULL),
(25, 1, 12, '2021-03-07 01:30:37', NULL, NULL),
(26, 1, 16, '2021-03-16 14:35:45', NULL, NULL),
(27, 2, 16, '2021-04-01 08:31:52', NULL, NULL),
(28, 2, 12, '2021-04-01 08:39:55', NULL, NULL),
(29, 3, 16, '2021-04-02 15:16:36', NULL, NULL),
(30, 3, 12, '2021-04-02 15:21:01', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_pangkat_master`
--

CREATE TABLE `tabel_pangkat_master` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_pangkat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pendidikan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tabel_pangkat_master`
--

INSERT INTO `tabel_pangkat_master` (`id`, `nama_pangkat`, `keterangan`, `pendidikan`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Juru Muda / Ia', NULL, 'SD,SLTP,SLTA,DIII,S1,S2,S3', '2021-03-23 00:45:01', '2021-04-04 07:08:00', NULL),
(2, 'Juru Muda Tingkat I / Ib', NULL, 'SD,SLTP,SLTA,DIII,S1,S2,S3', '2021-03-23 00:45:45', '2021-04-04 07:08:12', NULL),
(3, 'Juru / Ic', NULL, 'SD,SLTP,SLTA,DIII,S1,S2,S3', '2021-03-23 00:47:06', NULL, NULL),
(4, 'Juru Tingkat I / Id', NULL, 'SD,SLTP,SLTA,DIII,S1,S2,S3', '2021-03-23 00:47:25', NULL, NULL),
(5, 'Pengatur Muda / IIa', NULL, 'SLTA,DIII,S1,S2,S3', '2021-03-23 00:47:39', '2021-04-07 10:23:46', NULL),
(6, 'Pengatur Muda Tingkat I / IIb', NULL, 'SLTA,DIII,S1,S2,S3', '2021-03-23 00:47:51', '2021-04-07 10:23:58', NULL),
(7, 'Pengatur / IIc', NULL, 'SLTP,SLTA,DIII,S1,S2,S3', '2021-03-23 00:48:05', '2021-04-04 00:24:28', NULL),
(8, 'Pengatur Tingkat I / IId', NULL, 'SLTP,SLTA,DIII,S1,S2,S3', '2021-03-23 00:48:19', NULL, NULL),
(9, 'Penata Muda / IIIa', NULL, 'SLTA,DIII,S1,S2,S3', '2021-03-23 00:48:47', NULL, NULL),
(10, 'Penata Muda Tingkat I / IIIb', NULL, 'SLTA,DIII,S1,S2,S3', '2021-03-23 00:49:04', '2021-03-23 00:49:22', NULL),
(11, 'Penata / IIIc', NULL, 'DIII,S1,S2,S3', '2021-03-23 00:49:31', NULL, NULL),
(12, 'Penata Tingkat I / IIId', NULL, 'S1,S2,S3', '2021-03-23 00:49:47', NULL, NULL),
(13, 'Pembina / IVa', NULL, 'S2,S3', '2021-03-23 00:50:00', NULL, NULL),
(14, 'Pembina Tingkat I / IVb', NULL, 'S2', '2021-03-23 00:50:11', NULL, NULL),
(15, 'Pembina Utama Muda / IVc', NULL, NULL, '2021-03-23 00:51:13', NULL, NULL),
(16, 'Pembina Utama Madya / IVd', NULL, NULL, '2021-03-23 00:51:25', '2021-04-03 08:49:16', NULL),
(17, 'Pembina Utama / IVe', 'max', NULL, '2021-03-23 00:51:36', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_pengajuan_cuti`
--

CREATE TABLE `tabel_pengajuan_cuti` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode_cuti` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_cuti` date DEFAULT NULL,
  `tanggal_selesai_cuti` date DEFAULT NULL,
  `jumlah_hari_cuti` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alasan_cuti` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_handphone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat_cuti` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_cuti_id` int(11) DEFAULT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tabel_pengajuan_cuti`
--

INSERT INTO `tabel_pengajuan_cuti` (`id`, `kode_cuti`, `kode_user`, `tanggal_cuti`, `tanggal_selesai_cuti`, `jumlah_hari_cuti`, `alasan_cuti`, `no_handphone`, `alamat_cuti`, `description`, `jenis_cuti_id`, `read_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'CT1', 'KD1', '2021-02-23', '2021-02-27', '4', 'akjfkaf', '279', NULL, 'lkjadlkfadg', NULL, NULL, '2021-02-23 13:59:00', NULL, NULL),
(2, 'CT2', 'KD1', '2021-02-25', '2021-02-28', '3', 'testing', '2359819357193857', 'testing', 'testing', NULL, NULL, '2021-02-25 15:17:19', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_riwayat_pangkat`
--

CREATE TABLE `tabel_riwayat_pangkat` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pangkat_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_user` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tmt` date DEFAULT NULL,
  `tmt_selanjutnya` date DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tabel_riwayat_pangkat`
--

INSERT INTO `tabel_riwayat_pangkat` (`id`, `pangkat_id`, `kode_user`, `tmt`, `tmt_selanjutnya`, `keterangan`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1', 'KD3', '2021-04-14', '2021-01-04', 'PNS', '2021-02-14 14:16:26', '2021-02-15 12:53:40', NULL),
(2, '2', 'KD3', '2018-03-23', '2021-05-06', NULL, '2021-03-23 06:03:18', NULL, NULL),
(7, '1', 'KD2', '2018-03-23', '2021-05-09', NULL, '2021-03-23 06:03:18', NULL, NULL),
(12, '1', 'KD4', '2018-03-23', '2021-05-11', NULL, '2021-03-23 06:03:18', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_schedule_naik_gaji`
--

CREATE TABLE `tabel_schedule_naik_gaji` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tabel_schedule_naik_gaji`
--

INSERT INTO `tabel_schedule_naik_gaji` (`id`, `kode_user`, `tanggal`, `deleted_at`, `created_at`, `updated_at`) VALUES
(6, 'KD3', '2021-05-07', NULL, '2021-03-16 16:09:01', NULL),
(19, 'KD1', '2021-05-09', NULL, '2021-03-16 16:09:01', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_sisa_cuti`
--

CREATE TABLE `tabel_sisa_cuti` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tahun_cuti` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sisa_cuti` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tabel_sisa_cuti`
--

INSERT INTO `tabel_sisa_cuti` (`id`, `kode_user`, `tahun_cuti`, `sisa_cuti`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'KD1', '2020', '20', '2021-02-16 14:48:52', NULL, NULL),
(2, 'KD1', '2021', '12', '2021-02-16 15:00:07', NULL, NULL),
(3, 'KD1', '2023', '1', '2021-02-16 15:04:01', NULL, NULL),
(4, 'KD2', '2019', '12', '2021-02-16 15:05:11', NULL, NULL),
(5, 'KD1', '2021', '45', NULL, NULL, NULL),
(6, 'KD3', '2021', '12', NULL, NULL, NULL),
(7, 'KD4', '2021', '12', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_user_level`
--

CREATE TABLE `tabel_user_level` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tabel_user_level`
--

INSERT INTO `tabel_user_level` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'SuperAdmin', '2021-02-13 02:33:37', NULL, NULL),
(2, 'Admin', '2021-02-13 02:33:37', NULL, NULL),
(3, 'User', '2021-02-13 02:33:37', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_panggilan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kode_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_lengkap` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_pribadi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_kantor` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `tanggal_gabung` date DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_level` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_kelamin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tempat_lahir` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `agama_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_perkawinan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat_lama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat_baru` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_telepon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pendidikan_terakhir` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_pegawai_id` int(11) DEFAULT NULL,
  `npwp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `nama_panggilan`, `username`, `kode_user`, `nama_lengkap`, `nip`, `email_pribadi`, `email_kantor`, `email_verified_at`, `tanggal_gabung`, `password`, `secret`, `id_level`, `image`, `jenis_kelamin`, `tempat_lahir`, `tanggal_lahir`, `agama_id`, `status_perkawinan`, `alamat_lama`, `alamat_baru`, `no_telepon`, `pendidikan_terakhir`, `jenis_pegawai_id`, `npwp`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', 'admin1', 'KD1', 'Admin 123', '123', 'nurrohmannur110@gmail.com', 'admin@bpn.com', NULL, NULL, '$2y$10$jGZ76KYEKgQKyNw4fgzLRuCmeHha1DG33JuQWeO2hSSMv2AZmT8US', '123456', 1, '/storage/app/public/images/avatar/1616212908-KD1.jpeg', 'L', 'Demak', '2020-02-20', '1', NULL, 'lkfjlkdjfzlkjzdg', 'testing', '389751985', 'SLTA', 2, NULL, NULL, '2021-02-13 02:33:37', '2021-04-04 07:40:07', NULL),
(2, 'Rohman', NULL, 'KD2', 'Nur Rohman', '0819501', 'rex.one110@gmail.com', NULL, NULL, NULL, '$2y$10$qG4xGUUJJJ5h/Or6YL/FFemRtGMelGRkBt/FO59PEdHgABMJvov/K', '0819501', 3, NULL, 'L', 'Demak', '2000-02-11', '1', NULL, 'Tegalarum', NULL, '19085', 'SLTA', 1, NULL, NULL, '2021-02-17 14:22:59', NULL, '2021-02-18 13:39:43'),
(3, 'Admin', NULL, 'KD3', 'Admin', '12345678', 'rex.three110@gmail.com', NULL, NULL, NULL, '$2y$10$TWvnbowxqpT2Ks6K3lpzz.GzDkl4PmJk3jsJWIKKrPBiIXzwjtbK2', '12345678', 2, NULL, 'L', 'Demak', '2000-03-22', '1', NULL, 'ajfakdfakljdgadg', 'testing', '908431935', 'DIII', 1, NULL, NULL, '2021-03-04 11:21:52', '2021-04-04 07:40:22', NULL),
(4, 'User', NULL, 'KD4', 'User', '123456789', NULL, NULL, NULL, NULL, '$2y$10$G/irRo5oLL0O50Svunp69eQ15kt4uHtRnOEAdPkZEMrZJu6LRenKW', '123456789', 3, NULL, 'L', 'Demak', '2000-02-23', '1', NULL, 'aklfjalkdfad', 'Jl,Testing', '293857', 'SLTA', 1, NULL, NULL, '2021-03-04 11:22:37', '2021-04-03 08:42:24', NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `tabel_activity_harian`
--
ALTER TABLE `tabel_activity_harian`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kode_activity` (`kode_activity`);

--
-- Indeks untuk tabel `tabel_agama`
--
ALTER TABLE `tabel_agama`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tabel_detail_activity`
--
ALTER TABLE `tabel_detail_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tabel_jenis_cuti`
--
ALTER TABLE `tabel_jenis_cuti`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tabel_jenis_pegawai`
--
ALTER TABLE `tabel_jenis_pegawai`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tabel_modul`
--
ALTER TABLE `tabel_modul`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tabel_modul_akses`
--
ALTER TABLE `tabel_modul_akses`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tabel_pangkat_master`
--
ALTER TABLE `tabel_pangkat_master`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tabel_pengajuan_cuti`
--
ALTER TABLE `tabel_pengajuan_cuti`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tabel_pengajuan_cuti_kode_cuti_unique` (`kode_cuti`);

--
-- Indeks untuk tabel `tabel_riwayat_pangkat`
--
ALTER TABLE `tabel_riwayat_pangkat`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tabel_schedule_naik_gaji`
--
ALTER TABLE `tabel_schedule_naik_gaji`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tabel_sisa_cuti`
--
ALTER TABLE `tabel_sisa_cuti`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tabel_user_level`
--
ALTER TABLE `tabel_user_level`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_kode_user_unique` (`kode_user`),
  ADD UNIQUE KEY `users_nip_unique` (`nip`),
  ADD UNIQUE KEY `users_email_unique` (`email_pribadi`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email_kantor` (`email_kantor`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `tabel_activity_harian`
--
ALTER TABLE `tabel_activity_harian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `tabel_agama`
--
ALTER TABLE `tabel_agama`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tabel_detail_activity`
--
ALTER TABLE `tabel_detail_activity`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tabel_jenis_cuti`
--
ALTER TABLE `tabel_jenis_cuti`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tabel_jenis_pegawai`
--
ALTER TABLE `tabel_jenis_pegawai`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tabel_modul`
--
ALTER TABLE `tabel_modul`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `tabel_modul_akses`
--
ALTER TABLE `tabel_modul_akses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT untuk tabel `tabel_pangkat_master`
--
ALTER TABLE `tabel_pangkat_master`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `tabel_pengajuan_cuti`
--
ALTER TABLE `tabel_pengajuan_cuti`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tabel_riwayat_pangkat`
--
ALTER TABLE `tabel_riwayat_pangkat`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `tabel_schedule_naik_gaji`
--
ALTER TABLE `tabel_schedule_naik_gaji`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT untuk tabel `tabel_sisa_cuti`
--
ALTER TABLE `tabel_sisa_cuti`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `tabel_user_level`
--
ALTER TABLE `tabel_user_level`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
