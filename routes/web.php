<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ModulController;
use App\Http\Controllers\ManagementAccessController;
use App\Http\Controllers\PangkatController;
use App\Http\Controllers\RiwayatPangkatController;
use App\Http\Controllers\PegawaiController;
use App\Http\Controllers\SisaCutiController;
use App\Http\Controllers\PengajuanCutiController;
use App\Http\Controllers\ActivityHarianController;
use App\Http\Controllers\TypeCutiController;
use App\Http\Controllers\KenaikanGajiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/template',function(){
    return view('layout.template');
});
Route::get('/', function() {
    return view('secure.login');
})->name('login');

Route::post('/postLogin',[AuthController::class,'postLogin']);
Route::get('/logout',[AuthController::class,'logout']);

Route::group(['middleware' => ['auth','revalidate']], function () {
    // datatable API
    Route::get('/data/modul',[ModulController::class,'getModulData']);
    Route::get('/data/management-access',[ManagementAccessController::class,'getDataManagement']);
    Route::get('/data/pangkat',[PangkatController::class,'getPangkat']);
    Route::get('/data/getDataRiwayat',[RiwayatPangkatController::class,'getDataRiwayat']);
    Route::get('/data/data_pegawai',[PegawaiController::class,'data_pegawai']);
    Route::get('/data/sisa_cuti',[SisaCutiController::class,'dataSisaCuti']);
    Route::get('/data/pengajuan_cuti',[PengajuanCutiController::class,'data_pengajuan_cuti']);
    Route::get('/data/activity_harian',[ActivityHarianController::class,'getDataActivity']);
    Route::get('/data/get_kenaikan_gaji',[KenaikanGajiController::class,'getDataKenaikanGaji']);

    Route::get('/profile',[AuthController::class,'profile']);
    Route::post('/update_profile',[AuthController::class,'updateProfile']);
    Route::post('/updateFoto',[AuthController::class,'updateFoto']);
    Route::group(['middleware' => 'role'], function () {
        Route::get('/home',[HomeController::class,'index']);
        Route::prefix('pegawai')->group(function(){
        });

        Route::prefix('kenaikan-gaji')->group(function(){
            Route::get('/', [KenaikanGajiController::class,'index']);
            Route::post('/save_data',[KenaikanGajiController::class,'save']);
            Route::get('/get_kenaikan_gaji/{id}', [KenaikanGajiController::class,'get_kenaikan_gaji']);
            Route::post('/update_data/{id}',[KenaikanGajiController::class,'update_data']);
        });

        Route::prefix('pangkat')->group(function(){
            Route::get('/',[PangkatController::class,'index']);
            Route::post('/save',[PangkatController::class,'save']);
            Route::get('/gettingPangkat/{id}',[PangkatController::class,'gettingPangkat']);
            Route::post('/updatePangkat/{id}',[PangkatController::class,'updatePangkat']);
            Route::delete('/hapus/{id}',[PangkatController::class,'hapus']);
        });
        Route::prefix('activity-harian')->group(function(){
            Route::get('/',[ActivityHarianController::class,'index']);
            Route::post('/save_activity',[ActivityHarianController::class,'save_activity']);
            Route::get('/cetak_activity',[ActivityHarianController::class,'cetak_activity']);
            Route::get('/detailActivity/{id}',[ActivityHarianController::class,'detailActivity']);
            Route::get('/tambah',[ActivityHarianController::class,'tambah']);
        });
        Route::prefix('agama')->group(function(){

        });
        Route::prefix('type-cuti')->group(function(){
            // Route::get('/',)
        });
        Route::prefix('sisa_cuti')->group(function(){
            Route::get('/',[SisaCutiController::class,'index']);
            Route::post('/save',[SisaCutiController::class,'save_sisa_cuti']);
        });
        Route::prefix('pengajuan-cuti')->group(function(){
            Route::get('/',[PengajuanCutiController::class,'index']);
            Route::post('/save_pengajuan',[PengajuanCutiController::class,'save_data']);
            Route::get('/cetak_cuti/{id}',[PengajuanCutiController::class,'cetak_cuti']);
            Route::get('/detail/{id}',[PengajuanCutiController::class,'detailCuti']);
        });
        Route::prefix('pegawai')->group(function(){
            Route::get('/',[PegawaiController::class,'index']);
            Route::post('/save',[PegawaiController::class,'save']);
            Route::get('/detail_pegawai/{id}',[PegawaiController::class,'detail_pegawai']);
            Route::post('/update_pegawai/{id}',[PegawaiController::class,'update_pegawai']);
            Route::delete('/delete_pegawai/{id}',[PegawaiController::class,'delete_pegawai']);
        });
        Route::prefix('riwayat-pangkat')->group(function(){
            Route::get('/',[RiwayatPangkatController::class,'index']);
            Route::post('/save',[RiwayatPangkatController::class,'save']);
            Route::get('/getRiwayat/{id}',[RiwayatPangkatController::class,'getRiwayat']);
            Route::post('/prosesUpdate/{id}',[RiwayatPangkatController::class,'riwayatUpdate']);
        });
        Route::prefix('modul')->group(function(){
            Route::get('/', [ModulController::class,'index']);
            Route::delete('/hapus/{id}',[ModulController::class,'hapus']);
            Route::get('/getModul/{id}',[ModulController::class,'getModul']);
            Route::post('/save',[ModulController::class,'save']);
            Route::post('/update/{id}',[ModulController::class,'update']);
        });
        Route::prefix('management-access')->group(function(){
            Route::get('/',[ManagementAccessController::class,'index']);
            Route::post('/tambah',[ManagementAccessController::class,'tambah']);
            Route::get('/getManagement/{id}',[ManagementAccessController::class,'getManagement']);
            Route::post('/update/{id}',[ManagementAccessController::class,'update']);
            Route::delete('/hapus/{id}',[ManagementAccessController::class,'hapus']);
        });
    });
});
