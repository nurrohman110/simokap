<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use URL;
use Auth;
use DB;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // return $next($request);
        $role     = Auth::user()->id_level;
        $base_url = URL::to('/');
        $url      = url()->current();

        if ($url == url('/')) {
          $app_url = '/';
        } else {
          $get_main_url  = substr($url,strlen($base_url) + 1);
          $detect_slash = strpos($get_main_url,'/');
          if ($detect_slash > 0) {
            $explode_main_url = explode('/',$get_main_url);
            $app_url = $explode_main_url[0];
          } else {
            $app_url = $get_main_url;
          }
        }

        $get_modul = DB::table('tabel_modul')->where([
            ['url',$app_url],
            ['deleted_at',NULL]
        ])->first();
        if (isset($get_modul) == true) {
          $id_modul = $get_modul->id;
          $get_access = DB::table('tabel_modul_akses')->where([
                          ['id_level',$role],
                          ['id_modul',$id_modul],
                        ])->first();
          if (isset($get_access) == true) {
            return $next($request);
          } else {
            return abort(403);
          }
        } else {
          return abort(403);
        }
    }
}
