<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use PDF;

class PengajuanCutiController extends Controller
{
    public function index(){
        $getJenis = DB::table('tabel_jenis_cuti')
                ->where('deleted_at',NULL)
                ->get();
        $data = [
            'jenis' => $getJenis,
        ];
        return view('modul.pengajuan-cuti.index', $data);
    }

    public function data_pengajuan_cuti(){
        $level = Auth::user()->id_level;
        $kode_user = Auth::user()->kode_user;
        if($level == 3){
            $getPengajuan = DB::table('tabel_pengajuan_cuti')
                        ->where([
                            ['deleted_at',NULL],
                            ['kode_user',$kode_user]
                        ])
                        ->orderBy('id','desc')
                        ->get();
        }else{
            $getPengajuan = DB::table('tabel_pengajuan_cuti')
                        ->where('deleted_at',NULL)
                        ->orderBy('id','desc')
                        ->get();
        }
        if(count($getPengajuan) > 0){
            $no = 1;
            foreach($getPengajuan as $gp){
                $fetch = array();
                $fetch[] = $no++;
                $get_user = DB::table('users')
                        ->where('kode_user',$gp->kode_user)
                        ->first();
                // $fetch[] = isset($get_user) ? $get_user->nama_lengkap : '-';
                $fetch[] = $gp->tanggal_cuti;
                $fetch[] = $gp->tanggal_selesai_cuti;
                $fetch[] = $gp->alamat_cuti ?? '-';
                $fetch[] = $gp->jumlah_hari_cuti.' hari';
                $get_sisa_cuti = DB::table("tabel_sisa_cuti")
                            ->where([
                                ['kode_user',$gp->kode_user],
                                ['tahun_cuti',date('Y')]
                            ])
                            ->first();
                $fetch[] = isset($get_sisa_cuti) ? $get_sisa_cuti->sisa_cuti : '-';
                $btn = '
                        <div>
                            <button class="btn btn-sm btn-primary" onClick=detailPengajuanCuti("'.$gp->kode_cuti.'") title="Detail"><span class="fa fa-eye"></span> Detail</button>
                            <a href="pengajuan-cuti/cetak_cuti/'.$gp->kode_cuti.' " class="btn btn-sm btn-info" target="_blank" title="Detail"><span class="fa fa-print"></span> Cetak</a>
                        </div>
                        ';
                $fetch[] = $btn;

                $data[] =$fetch;
            }
        }else{
            $data = array();
        }
        $out = array("data" => $data);
        return response()->json($out);
    }

    public function save_data(Request $request){
        $kode_user = Auth::user()->kode_user;
        $tahun = date('Y');
        $jumlah_hari = $request->jumlah_hari;

        $getSisaCuti = DB::table('tabel_sisa_cuti')
                ->where([
                    ['kode_user',$kode_user],
                    ['tahun_cuti',$tahun],
                    ['deleted_at', NULL],
                ])
                ->first();
        $kodeArrayCuti = array();
        $getCuti = DB::table("tabel_pengajuan_cuti")->get();
        if(count($getCuti) > 0){
            foreach($getCuti as $gc){
                $subString = substr($gc->kode_cuti,2);
                $kodeArrayCuti[] = $subString;
            }
            $maxNumber = max($kodeArrayCuti) + 1;
            $kode_cuti = 'CT'.$maxNumber;
        }else{
            $kode_cuti = 'CT1';
        }
        $Date1 = $request->tanggal_mulai_cuti;
        $selesai_cuti = date('Y-m-d', strtotime($Date1 . " + ".$jumlah_hari." day"));
        if(isset($getSisaCuti)){
            $sisa_cuti = $getSisaCuti->sisa_cuti;
            $countSisaCuti = $sisa_cuti - $jumlah_hari;
            if($countSisaCuti < 0){
                $response = [
                    'value' => 1,
                    'msg' => 'sisa cuti tidak mencukupi',
                ];
            }else if($sisa_cuti == 0){
                $response = [
                    'value' => 2,
                    'msg' => 'Sisa cuti anda sudah habis'
                ];
            }else{
                $save = DB::table('tabel_pengajuan_cuti')
                    ->insert([
                        'kode_cuti' => $kode_cuti,
                        'kode_user' => $kode_user,
                        'tanggal_cuti' => $request->tanggal_mulai_cuti,
                        'tanggal_selesai_cuti' => $selesai_cuti,
                        'jumlah_hari_cuti' => $jumlah_hari,
                        'alasan_cuti' => $request->alasan_cuti,
                        'no_handphone' => $request->no_handphone,
                        'alamat_cuti' => $request->alamat_cuti,
                        'description' => $request->keterangan,
                        'created_at' => now(),
                    ]);
                $response = [
                    'value' => 3,
                    'msg' => 'Berhasil insert cuti',
                    'msg_code' => 200,
                ];
            }
        }else{
            $response = [
                'value' => 4,
                'msg' => 'Cuti belum di generate',
            ];
        }
        return response()->json($response);
    }

    public function detailCuti($id){
        $get_cuti = DB::table('tabel_pengajuan_cuti')
                ->where("kode_cuti",$id)
                ->first();
        return response()->json($get_cuti);
    }

    public function cetak_cuti($id){
        $get_cuti = DB::table('tabel_pengajuan_cuti')
                ->where('kode_cuti',$id)
                ->first();
        $get_user = DB::table('users')
                ->where('kode_user',$get_cuti->kode_user)
                ->first();
        $get_pangkat = DB::table('tabel_riwayat_pangkat')
                    ->where('kode_user',$get_cuti->kode_user)
                    ->first();
        if(isset($get_pangkat)){
            $get_jabatan = DB::table('tabel_pangkat_master')
                    ->where('id',$get_pangkat->pangkat_id)
                    ->first();
            $jabatan = $get_jabatan->nama_pangkat;
        }else{
            $jabatan = '-';
        }
        $data = [
            'user' => $get_user,
            'cuti' => $get_cuti,
            'jabatan' => $jabatan,
        ];
        $pdf = PDF::loadView('modul.pengajuan-cuti.laporan', $data);
        $pdf->setPaper('A4', 'potrait');
        return $pdf->stream('Report.pdf', array('Attachment' => 0));
    }
}
