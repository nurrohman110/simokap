<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ModulController extends Controller
{
    public function index(){
        return view('modul.modul-akses.index');
    }

    public function getModulData(){
        $getModul = DB::table('tabel_modul')
                ->where('deleted_at',NULL)
                ->get();
        if(count($getModul) > 0){
            $no = 1;
            foreach($getModul as $gm){
                $fetch = array();
                $fetch[] = $no++;
                $fetch[] = $gm->nama_modul;
                $fetch[] = $gm->url;
                $fetch[] = $gm->icon.' (<span class="'.$gm->icon.'"></span>)';
                $fetch[] = '
                            <button onclick=updateModul("'.$gm->id.'") class="btn-sm btn btn-primary"><span class="fa fa-edit"></span> Update</button>
                            <button onclick=deleteModul("'.$gm->id.'") class="btn-sm btn btn-danger"><span class="fa fa-trash-o"></span> Delete</button>
                        ';
                $data[] = $fetch;
            }
        }else{
            $data = array();
        }
        $output = array("data" => $data);
        return response()->json($output);
    }
    public function hapus($id){
        $udpate = DB::table('tabel_modul')
                ->where('id',$id)
                ->update([
                    'deleted_at' => now(),
                ]);
        return response()->json($udpate);
    }

    public function save(Request $request){
        $unsert = DB::table('tabel_modul')
                ->insert([
                    'nama_modul' => $request->nama_modul,
                    'url' => $request->url_name,
                    'icon' => $request->icon,
                    'created_at' => now(),
                ]);
    }

    public function update(Request $request, $id){
        $update = DB::table('tabel_modul')
                ->where('id',$id)
                ->update([
                    'nama_modul' => $request->nama_modul,
                    'url' => $request->url_name,
                    'icon' => $request->icon,
                    'updated_at' => now(),
                ]);
    }

    public function getModul($id){
        $getModul = DB::table('tabel_modul')
                    ->where('id',$id)
                    ->first();
        return response()->json($getModul);
    }
}
