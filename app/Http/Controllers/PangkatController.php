<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PangkatController extends Controller
{
    public function index(){
        return view('modul.pangkat.index');
    }

    public function getPangkat(){
        $getPangkat = DB::table('tabel_pangkat_master')
                ->where("deleted_at",NULL)
                ->get();
        if(count($getPangkat) > 0){
            $no = 1;
            foreach($getPangkat as $gp){
                $fetch = array();
                $fetch[] = $no++;
                $fetch[] = $gp->nama_pangkat;
                $fetch[] = $gp->pendidikan;
                $btn = '<button onclick=updatePangkat("'.$gp->id.'") class="btn-sm btn btn-primary"><span class="fa fa-edit"></span> Update</button>
                        <button onclick=deletePangkat("'.$gp->id.'") class="btn-sm btn btn-danger"><span class="fa fa-trash-o"></span> Delete</button>
                        ';
                $fetch[] = $btn;
                $data[] =$fetch;
            }
        }else{
            $data = array();
        }
        $output = array("data" => $data);
        return response()->json($output);
    }

    public function save(Request $request){
        $pendidikan = implode(',',$request->pendidikan);
        $insert = DB::table('tabel_pangkat_master')
                ->insert([
                    'nama_pangkat' => $request->nama_pangkat,
                    'pendidikan' => $pendidikan,
                    'created_at' => now()
                ]);
    }

    public function gettingPangkat($id){
        $getPangkat = DB::table('tabel_pangkat_master')
                ->where('id',$id)
                ->first();

        $test[] = $getPangkat->pendidikan;
        $d = array();
        foreach($test as $t){
            $d[] = explode(',',$t);
        }
        $getPangkat->testing = $d;
        return response()->json($getPangkat);
    }

    public function updatePangkat(Request $request, $id){
        $pendidikan = implode(',',$request->pendidikan);
        $update = DB::table('tabel_pangkat_master')
                ->where('id',$id)
                ->update([
                    'nama_pangkat' => $request->nama_pangkat,
                    'pendidikan' => $pendidikan,
                    'updated_at' => now()
                ]);
    }

    public function hapus(Request $request, $id){
        $delete = DB::table('tabel_pangkat_master')
                ->where('id',$id)
                ->update([
                    'deleted_at' => now(),
                ]);
    }
}
