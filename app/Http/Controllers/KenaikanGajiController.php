<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class KenaikanGajiController extends Controller
{
    public function index(){
        $get_pegawai = DB::table('users')
                    ->where('deleted_at',NULL)
                    ->get();
        $data = [
            'pegawai' => $get_pegawai,
        ];
        return view('modul.kenaikan-gaji.index',$data);
    }

    public function getDataKenaikanGaji(Request $request){
        $kode_user = Auth::user()->kode_user;
        $level = Auth::user()->id_level;
        if($level == 3){
            $get_kenaikan_gaji = DB::table('tabel_schedule_naik_gaji')
                        ->where([
                            ['deleted_at',NULL],
                            ['kode_user',$kode_user]
                        ])
                        ->orderBy('id','desc')
                        ->get();
        }else{
            // if($request->kode_user != ''){
            $get_kenaikan_gaji = DB::table('tabel_schedule_naik_gaji')
                    ->where([
                        ['deleted_at',NULL],
                        ['tanggal','LIKE',$request->tahunResult.'%'],
                        ['tanggal','LIKE','%'.$request->bulanResult.'%'],
                    ])
                    ->orderBy('id','desc')
                    ->get();
            // }else{
            //     $get_kenaikan_gaji = DB::table('tabel_schedule_naik_gaji')
            //             ->where([
            //                 ['deleted_at',NULL],
            //             ])
            //             ->orderBy('id','desc')
            //             ->get();
            // }
        }
        if(count($get_kenaikan_gaji) > 0){
            $no = 1;
            foreach($get_kenaikan_gaji as $gk){
                $fetch = array();
                $fetch[] = $no++;
                $fetch[] = nama_lengkap($gk->kode_user);
                $fetch[] = $gk->tanggal;
                if($level == 1){
                    $btn = '
                            <button class="btn btn-sm btn-primary" onclick=updateSchedule('.$gk->id.')><span class="fa fa-print"></span> Update</button>
                            <button class="btn btn-sm btn-danger"><span class="fa fa-trash-o"></span> Delete</button>
                        ';
                    $fetch[] = $btn;
                }
                $data[] = $fetch;
            }
        }else{
            $data = array();
        }
        $output = array("data" => $data);
        return response()->json($output);
    }

    public function save(Request $request){
        $insert = DB::table('tabel_schedule_naik_gaji')
                ->insert([
                    'kode_user' => $request->nama_pegawai,
                    'tanggal' => $request->tanggal,
                    'created_at' => now(),
                ]);
    }

    public function get_kenaikan_gaji($id){
        $get_kenaikan_gaji = DB::table('tabel_schedule_naik_gaji')
                    ->where('id',$id)
                    ->first();
        return response()->json($get_kenaikan_gaji);
    }

    public function update_data(Request $request, $id){
        $update = DB::table('tabel_schedule_naik_gaji')
                ->where('id',$id)
                ->update([
                    'kode_user' => $request->nama_pegawai,
                    'tanggal' => $request->tanggal,
                    'updated_at' => now(),
                ]);
    }
}
