<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use PDF;

class ActivityHarianController extends Controller
{
    public function index(){
        $get_pegawai = DB::table('users')
                    ->where('deleted_at',NULL)
                    ->get();
        $data = [
            'pegawai' => $get_pegawai,
        ];
        return view('modul.activity-harian.index', $data);
    }

    public function getDataActivity(Request $request){
        $level = Auth::user()->id_level;
        $kode_user = Auth::user()->kode_user;
        if($level == 3){
            $getActivity = DB::table('tabel_activity_harian')
                        ->where([
                            ['kode_user',$kode_user],
                            ['kode_activity','!=',NULL],
                        ])
                        ->orderBy("tanggal",'desc')
                        ->whereBetween('tanggal',[$request->mulai, $request->selesai])
                        ->get();
        }else{
            if($request->kode_user != ''){
                $getActivity = DB::table("tabel_activity_harian")
                            ->where([
                                ['kode_user',$request->kode_user],
                                ['kode_activity','!=',NULL],
                            ])
                        ->orderBy("tanggal",'desc')
                        ->whereBetween('tanggal',[$request->mulai, $request->selesai])
                        ->get();
            }else{
                $getActivity = DB::table("tabel_activity_harian")
                        ->where('kode_activity','!=',NULL)
                        ->orderBy("tanggal",'desc')
                        ->whereBetween('tanggal',[$request->mulai, $request->selesai])
                        ->get();
            }

        }

        if(count($getActivity) > 0){
            $no = 1;
            foreach($getActivity as $ga){
                $fetch = array();
                $fetch[] = $no++;
                $get_user = DB::table('users')
                        ->where('kode_user',$ga->kode_user)
                        ->first();
                $fetch[] = $ga->tanggal;
                $get_uraian = DB::table('tabel_activity_harian')
                            ->where('parent_kode_activity', $ga->kode_activity)
                            ->get();
                $arr = array();
                $no = 1;
                foreach($get_uraian as $gu){
                    $arr[] = $no++.'. '. $gu->description.' </br>';
                }
                $fetch[] =  implode('',$arr);
                $kode_user = encrypt($ga->kode_user);
                if($level != 3){
                    $btn = '
                            <button class="btn btn-sm btn-danger"><span class="fa fa-trash-o"></span> Delete</button>
                            <buton class="btn btn-sm btn-info" onclick=detailActivity("'.$ga->kode_activity.'")><span class="fa fa-eye"></span> Detail</button>
                            ';
                }else{
                    $btn = '
                        <buton class="btn btn-sam btn-info" onclick=detailActivity("'.$ga->kode_activity.'")><span class="fa fa-eye"></span> Detail</button>
                        ';
                }
                $fetch[] = $btn;
                $data[] = $fetch;
            }
        }else{
            $data = array();
        }
        $out = array(
            "data" => $data,
            'mulai' => $request->mulai,
            'selesai' => $request->selesai,
        );
        return response()->json($out);
    }

    public function save_activity(Request $request){
        $getActivity = DB::table('tabel_activity_harian')->get();
        $maxNumber = array();
        if(count($getActivity) > 0){
            foreach($getActivity as $ga){
                $subString = substr($ga->kode_activity, 2);
                $maxNumber[] = $subString;
            }
            $kodeMax = max($maxNumber) + 1;
            $kode_activity = 'AH'.$kodeMax;
        }else{
            $kode_activity = 'AH1';
        }

        $insert_induk = DB::table('tabel_activity_harian')
                ->insert([
                    'kode_activity' => $kode_activity,
                    'kode_user' => Auth::user()->kode_user,
                    'tanggal' => $request->tanggal,
                    'created_at' => now(),
                ]);
        $array_index = 0;
        foreach($request->activity as $kb){
            if($kb != null){
                $save = DB::table('tabel_activity_harian')
                        ->insert([
                            'parent_kode_activity' => $kode_activity,
                            'kode_user' => Auth::user()->kode_user,
                            'tanggal' => $request->tanggal,
                            'description' => $kb,
                            'created_at' => now(),
                        ]);
            }
            $array_index++;
        }
        return redirect('activity-harian');
    }

    public function tambah(){
        return view('modul.activity-harian.tambah');
    }

    public function cetak_activity(Request $request){
        $kode_user = decrypt($request->kode_user);
        $get_user = DB::table('users')
                ->where('kode_user',$kode_user)
                ->first();
        $get_activity = DB::table('tabel_activity_harian')
                    ->where([
                        ['kode_user',$kode_user],
                        ['kode_activity','!=',NULL]
                    ])
                    ->whereBetween('tanggal',[$request->mulai, $request->selesai])
                    ->get();
        if(count($get_activity) > 0){
            $no = 1;
            foreach($get_activity as $ga){
                $fetch = array();
                $fetch['nomor'] = $no++;
                $user = DB::table('users')
                        ->where('kode_user',$kode_user)
                        ->first();
                $timestamp = strtotime($ga->tanggal);
                $weekday= date("l", $timestamp );
                $normalized_weekday = strtolower($weekday);
                echo $normalized_weekday ;
                if (($normalized_weekday == "saturday") || ($normalized_weekday == "sunday")) {
                    $fetch['status_tanggal'] = true;
                } else {
                    $fetch['status_tanggal'] = false;
                }
                $fetch['nama_lengkap'] = $user->nama_lengkap;
                $fetch['tanggal'] = tanggal($ga->tanggal);
                $get_uraian = DB::table('tabel_activity_harian')
                            ->where('parent_kode_activity', $ga->kode_activity)
                            ->get();
                $arr = array();
                $no = 1;
                foreach($get_uraian as $gu){
                    $arr[] = $no++.'. '. $gu->description.'<br> <br>';
                }
                $fetch['description'] =  implode('',$arr);
                $activity[] = $fetch;
            }
        }else{
            $activity = array();
        }
        $data = [
            'activity' => $activity,
            'user' => $get_user,
            'mulai' => tanggal($request->mulai),
            'selesai' => tanggal($request->selesai),
            'nama' => nama_lengkap($kode_user),
        ];

        $pdf = PDF::loadView('modul.activity-harian.report', $data);
        $pdf->setPaper('A4', 'potrait');
        return $pdf->stream('Report.pdf', array('Attachment' => 0));
    }

    public function detailActivity($id){
        $get_activity = DB::table('tabel_activity_harian')
                ->where([
                    ['kode_activity',$id],
                ])
                ->get();
        foreach($get_activity as $ga){
            $fetch = array();
            $fetch['tanggal'] = $ga->tanggal;

            $get_uraian = DB::table('tabel_activity_harian')
                            ->where('parent_kode_activity', $ga->kode_activity)
                            ->get();
            $arr = array();
            $no = 1;
            foreach($get_uraian as $gu){
                $arr[] = $no++.'. '. $gu->description.' <br>';
            }
            $fetch['description'] =  implode('',$arr);
            $data[] = $fetch;
        }
        $tes = [
            'data' => $data,
        ];
        return response()->json($tes);
    }
}
