<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ManagementAccessController extends Controller
{
    public function index(){
        $level = DB::table('tabel_user_level')->where('deleted_at',NULL)->get();
        $modul = DB::table('tabel_modul')->where('deleted_at',NULL)->get();

        $data = [
            'level' => $level,
            'modul' => $modul
        ];
        return view('modul.management-access.index', $data);
    }

    public function getDataManagement(){
        $getManagement = DB::table('tabel_modul_akses')
                    ->where('deleted_at',NULL)
                    ->get();

        if(count($getManagement) > 0){
            $no = 1;
            foreach($getManagement as $gm){
                $fetch = array();
                $fetch[] = $no++;
                $fetch[] = level($gm->id_level);
                $fetch[] = modul($gm->id_modul);
                $fetch[] = '
                            <button onclick=updateManagement("'.$gm->id.'") class="btn-sm btn btn-primary"><span class="fa fa-edit"></span> Update</button>
                            <button onclick=deleteManagement("'.$gm->id.'") class="btn-sm btn btn-danger"><span class="fa fa-trash-o"></span> Delete</button>
                        ';
                $data[] = $fetch;
            }
        }else{
            $data = array();
        }
        $output = array("data" => $data);
        return response()->json($output);
    }

    public function tambah(Request $request){
        $insert = DB::table('tabel_modul_akses')
                ->insert([
                    'id_level' => $request->level,
                    'id_modul' => $request->modul,
                    'created_at' => now(),
                ]);
    }

    public function getManagement($id){
        $getManagement = DB::table('tabel_modul_akses')
                    ->where('id',$id)
                    ->first();
        return response()->json($getManagement);
    }

    public function update(Request $request, $id){
        $update = DB::table('tabel_modul_akses')
                ->where('id',$id)
                ->update([
                    'id_level' => $request->level,
                    'id_modul' => $request->modul,
                    'updated_at' => now(),
                ]);
    }

    public function hapus(Request $request, $id){
        $delete = DB::table('tabel_modul_akses')
                ->where('id',$id)
                ->update([
                    'deleted_at' => now(),
                ]);
    }
}
