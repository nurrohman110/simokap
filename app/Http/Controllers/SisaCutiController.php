<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class SisaCutiController extends Controller
{
    public function index(){
        $getPegawai = DB::table('users')
                    ->where('deleted_at',NULL)
                    ->get();
        $data = [
            'pegawai' => $getPegawai,
        ];
        return view('modul.sisa_cuti.index',$data);
    }

    public function dataSisaCuti(){
        $level = Auth::user()->id_level;
        $kodeUser = Auth::user()->kode_user;
        if($level == 3){
            $getSisaCuti = DB::table('tabel_sisa_cuti')
                        ->where([
                            ['deleted_at',NULL],
                            ['kode_user',$kodeUser]
                        ])
                        ->orderBy('tahun_cuti','desc')
                        ->get();
        }else{
            $getSisaCuti = DB::table('tabel_sisa_cuti')
                        ->where([
                            ['deleted_at',NULL],
                        ])
                        ->orderBy('tahun_cuti','desc')
                        ->get();
        }
        if(count($getSisaCuti) > 0){
            $no = 1;
            foreach($getSisaCuti as $gs){
                $fetch = array();
                $fetch[] = $no++;
                $getUser = DB::table('users')
                        ->where('kode_user',$gs->kode_user)
                        ->first();
                $fetch[] = $getUser->nama_lengkap;
                $fetch[] = $gs->tahun_cuti;
                $fetch[] = $gs->sisa_cuti;

                $data[] = $fetch;
            }
        }else{
            $data = array();
        }
        $out = array("data" => $data);
        return response()->json($out);
    }

    public function save_sisa_cuti(Request $request){
        $cek = DB::table('tabel_sisa_cuti')
                ->where([
                    ['kode_user',$request->kode_user],
                    ['tahun_cuti',$request->tahun]
                ])
                ->first();
        if(!$cek){
            $save = DB::table('tabel_sisa_cuti')
                    ->insert([
                        'kode_user' => $request->kode_user,
                        'tahun_cuti' => $request->tahun,
                        'sisa_cuti' => $request->sisa_cuti,
                        'created_at' => now(),
                    ]);
        }else{
            $save = false;
        }
        return response()->json($save);
    }
}
