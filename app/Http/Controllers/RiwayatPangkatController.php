<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class RiwayatPangkatController extends Controller
{
    public function index(){
        $pangkat = DB::table('tabel_pangkat_master')
                ->where('deleted_at',NULL)
                ->get();
        $pegawai = DB::table('users')
                ->where([
                    ['deleted_at',NULL],
                ])
                ->get();
        $data = [
            'pangkat' => $pangkat,
            'pegawai' => $pegawai,
        ];
        return view('modul.riwayat-pangkat.index', $data);
    }

    public function getDataRiwayat(Request $request){
        $level = Auth::user()->id_level;
        $kodeUser = Auth::user()->kode_user;
        if($level == 3){
            $getRiwayat = DB::table('tabel_riwayat_pangkat')
                        ->where([
                            ['deleted_at',NULL],
                            ['kode_user',$kodeUser],
                            ['tmt_selanjutnya','LIKE',$request->tahunResult.'%'],
                            ['tmt_selanjutnya','LIKE','%'.$request->bulanResult.'%'],
                        ])
                        ->get();
        }else{
            $getRiwayat = DB::table('tabel_riwayat_pangkat')
                        ->where([
                            ['deleted_at',NULL],
                            ['tmt_selanjutnya','LIKE',$request->tahunResult.'%'],
                            ['tmt_selanjutnya','LIKE','%'.$request->bulanResult.'%'],
                        ])
                        ->get();
        }
        if(count($getRiwayat) > 0){
            $no = 1;
            foreach($getRiwayat as $gr){
                $fetch = array();
                $fetch[] = $no++;
                $fetch[] = nama_lengkap($gr->kode_user);
                $fetch[] = pangkat($gr->pangkat_id);
                $fetch[] = $gr->tmt;
                $fetch[] = $gr->tmt_selanjutnya;
                $fetch[] = $gr->keterangan;
                $btn = '
                        <div class="form-group center">
                            <button onclick=updateData("'.$gr->id.'") class="btn-sm btn btn-primary"><span class="fa fa-pencil"></span> Update</button>
                        </div>
                        ';
                $fetch[] = $btn;
                $data[] = $fetch;
            }
        }else{
            $data = array();
        }
        $output = array(
            "data" => $data,
            'tahun' => $request->tahunResult,
            'bulan' => $request->bulanResult,
        );
        return response()->json($output);
    }

    public function save(Request $request){
        $tmt = $request->tmt;
        $next_tmt = date('Y-m-d', strtotime($tmt. ' + 4 years'));
        $save = DB::table('tabel_riwayat_pangkat')
                ->insert([
                    'kode_user' => $request->kode_user,
                    'pangkat_id' => $request->pangkat_id,
                    'tmt' => $request->tmt,
                    'tmt_selanjutnya' => $next_tmt,
                    'keterangan' => $request->keterangan,
                    'created_at' => now(),
                ]);
    }

    public function getRiwayat($id){
        $getRiwayat = DB::table('tabel_riwayat_pangkat')
                ->where('id',$id)
                ->first();
        return response()->json($getRiwayat);
    }

    public function riwayatUpdate(Request $request, $id){
        $tmt = $request->tmt;
        $next_tmt = date('Y-m-d', strtotime($tmt. ' + 4 years'));
        $save = DB::table('tabel_riwayat_pangkat')
                ->where('id',$id)
                ->update([
                    'kode_user' => $request->kode_user,
                    'pangkat_id' => $request->pangkat_id,
                    'tmt' => $request->tmt,
                    'tmt_selanjutnya' => $next_tmt,
                    'keterangan' => $request->keterangan,
                    'updated_at' => now(),
                ]);
    }
}
