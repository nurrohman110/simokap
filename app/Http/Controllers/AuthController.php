<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Str;
use File;
use Image;

class AuthController extends Controller
{
    public function postLogin(Request $request){
        $credentials = $request->only('nip', 'password');

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            $home = redirect()->intended('/home');
            return $home->header('Cache-Control','nocache, no-store, max-age=0, must-revalidate')
                    ->header('Pragma','no-cache')
                    ->header('Expires','Fri, 01 Jan 1990 00:00:00 GMT');
        }
        return redirect()->intended('/')->with('salah','Maaf, Nip atau Password anda salah');
    }
    public function logout(Request $request) {
        Auth::logout();
        return redirect('/');
    }

    public function profile(){
        return view('secure.profile');
    }

    public function updateProfile(Request $request){
        $kode_user = Auth::user()->kode_user;
        $update =  DB::table('users')
                ->where('kode_user',$kode_user)
                ->update([
                    'nama_lengkap' => $request->nama_lengkap,
                    'email_pribadi' => $request->email_pribadi,
                    'email_kantor' => $request->email_kantor,
                    'tempat_lahir' => $request->tempat_lahir,
                    'tanggal_lahir' => $request->tanggal_lahir,
                    'password' => bcrypt($request->password),
                    'secret' => $request->password,
                    'alamat_lama' => $request->alamat_lama,
                    'alamat_baru' => $request->alamat_baru,
                    'no_telepon' => $request->nomor_telepon,
                    'updated_at' => now(),
                ]);
        if($update){
            return redirect('/profile')->with('success','Success!');
        }else{
            return redirect('/profile')->with('gagal','Gagal!!');
        }
    }

    public function updateFoto(Request $request){
        // $get_user = DB::table('users')
        //         ->where('kode_user',Auth::user()->kode_user)
        //         ->first();
        $imageName = $request->file('image_profile');

        if($imageName != null)
        {
            $fieldFile = $request->file('image_profile');
            $mime= $fieldFile->getClientOriginalExtension();
            $imageName = time().'-'.Auth::user()->kode_user.".".$mime;
            $image = Image::make($fieldFile)->resize(188, 187);
            $tes = \Storage::disk('public')->put("images/avatar/".$imageName,  $image->stream());
            $update = DB::table('users')
                    ->where('kode_user',Auth::user()->kode_user)
                    ->update([
                        'image' => '/storage/app/public/images/avatar/'.$imageName,
                    ]);
            if($update){
                return redirect('/profile')->with('success','Success!');
            }else{
                return redirect('/profile')->with('gagal','Gagal!');
            }
        }else{
            return redirect('/profile')->with('info','Info!');
        }

    }
}
