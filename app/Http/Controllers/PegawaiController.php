<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Auth;

class PegawaiController extends Controller
{
    public function index(){
        $getJenis = DB::table('tabel_jenis_pegawai')->get();
        if(Auth::user()->id_level == 1){
            $getLevel = DB::table('tabel_user_level')
                    ->where([
                        ['deleted_at',NULL],
                    ])
                    ->get();
        }else{
            $getLevel = DB::table('tabel_user_level')
                    ->where([
                        ['deleted_at',NULL],
                        ['id','!=',1],
                    ])
                    ->get();
        }
        $agama = DB::table('tabel_agama')
                ->get();
        $data = [
            'jenis' => $getJenis,
            'level' => $getLevel,
            'agama' => $agama,
        ];
        return view('modul.pegawai.index', $data);
    }

    public function data_pegawai(){
        $getPegawai = DB::table('users')
                    ->where('deleted_at',NULL)
                    ->get();
        if(count($getPegawai) > 0){
            $no = 1;
            foreach($getPegawai as $gp){
                $fetch = array();
                $fetch[] = $no++;
                $fetch[] = $gp->kode_user;
                $fetch[] = $gp->nama_lengkap;
                $fetch[] = $gp->nama_panggilan;
                $fetch[] = $gp->nip;
                $fetch[] = level($gp->id_level);
                $fetch[] = $gp->jenis_kelamin == 'L' ? 'Laki-Laki' : 'Perempuan';
                $fetch[] = $gp->tempat_lahir.' '.$gp->tanggal_lahir;
                $fetchAgama = DB::table('tabel_agama')
                            ->where('id',$gp->agama_id)
                            ->first();
                // $fetch[] = isset($fetchAgama) ? $fetchAgama->nama_agama : '-';
                $fetch[] = $gp->no_telepon ?? 0;
                $btn = '
                        <button class="btn-sm btn btn-info" title="Detail" onclick=detailPegawai("'.$gp->kode_user.'")><span class="fa fa-eye"></span></button>
                        <button class="btn-sm btn btn-primary" onclick=updatePegawai("'.$gp->kode_user.'") title="Update"><span class="fa fa-pencil"></span></button>
                        <button class="btn-sm btn btn-danger" onclick=hapusPegawai("'.$gp->kode_user.'") title="Hapus"><span class="fa fa-trash-o"></span></button>
                        ';
                $fetch[] = $btn;

                $data[] = $fetch;
            }
        }else{
            $data = array();
        }
        $output = array("data" => $data);
        return response()->json($output);
    }

    public function save(Request $request){
        $getPegawai = DB::table('users')->get();
        $kodeArray = array();
        if(count($getPegawai) > 0){
            foreach($getPegawai as $gp){
                $subString = substr($gp->kode_user, 2);
                $kodeArray[] = $subString;
            }
            $numberMax = max($kodeArray) +1;
            $kodeUser = 'KD'.$numberMax;
        }else{
            $kodeUser = 'KD1';
        }

        $save = DB::table('users')
            ->insert([
                'kode_user' => $kodeUser,
                'nama_lengkap' => $request->nama_lengkap,
                'nama_panggilan' => $request->nama_panggilan,
                'nip' => $request->nip,
                'jenis_kelamin' => $request->jenis_kelamin,
                'tempat_lahir' => $request->tempat_lahir,
                'password' => bcrypt($request->nip),
                'secret' => $request->nip,
                'id_level' => $request->level,
                'tanggal_lahir' => $request->tanggal_lahir,
                'agama_id' => $request->agama,
                'jenis_pegawai_id' => $request->type_pegawai,
                'no_telepon' => $request->nomor_telepon,
                'npwp' => $request->npwp,
                'pendidikan_terakhir' => $request->pendidikan_terakhir,
                'alamat_baru' => $request->alamat,
                'created_at' => now(),
            ]);
    }

    public function detail_pegawai($id){
        $detail = DB::table('users')
                ->where('kode_user',$id)
                ->first();
        $detail->role = level($detail->id_level);
        $detail->kelamin = $detail->jenis_kelamin == 'L' ? 'Laki-Laki' : 'Perempuan';
        $getAgama = DB::table('tabel_agama')
                ->where('id',$detail->agama_id)
                ->first();
        $detail->agama_detail = $getAgama->nama_agama;
        $getStatus = DB::table('tabel_jenis_pegawai')
                ->where('id',$detail->jenis_pegawai_id)
                ->first();
        $detail->status_pegawai = isset($getStatus) ? $getStatus->nama_jenis : '-';

        return response()->json($detail);
    }

    public function update_pegawai(Request $request, $id){
        $update = DB::table('users')
                ->where('kode_user',$id)
                ->update([
                    'nama_lengkap' => $request->nama_lengkap,
                    'nama_panggilan' => $request->nama_panggilan,
                    'nip' => $request->nip,
                    'jenis_kelamin' => $request->jenis_kelamin,
                    'tempat_lahir' => $request->tempat_lahir,
                    'id_level' => $request->level,
                    'tanggal_lahir' => $request->tanggal_lahir,
                    'agama_id' => $request->agama,
                    'jenis_pegawai_id' => $request->type_pegawai,
                    'no_telepon' => $request->nomor_telepon,
                    'npwp' => $request->npwp,
                    'pendidikan_terakhir' => $request->pendidikan_terakhir,
                    'alamat_baru' => $request->alamat,
                    'updated_at' => now(),
                ]);
    }

    public function delete_pegawai($id){
        $delete = DB::table('users')
                ->where('kode_user',$id)
                ->update([
                    'deleted_at' => now(),
                ]);
    }
}
