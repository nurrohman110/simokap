<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

class HomeController extends Controller
{
    public function index(){
        $kode_user = Auth::user()->kode_user;
        $get_sisa_cuti = DB::table('tabel_sisa_cuti')
                    ->where([
                        ['kode_user',$kode_user],
                        ['tahun_cuti', date('Y')]
                    ])
                    ->first();
        $naik_gaji = DB::table('tabel_schedule_naik_gaji')
                    ->where([
                        ['kode_user',$kode_user],
                        ['deleted_at',NULL]
                    ])
                    ->latest()
                    ->first();
        $get_pangkat = DB::table('tabel_riwayat_pangkat')
                    ->where([
                        ['kode_user',$kode_user],
                        ['deleted_at',NULL]
                    ])
                    ->latest()
                    ->first();
        $str_minus = date('Y-m-d',strtotime('-1 days'));
        $data = [
            'sisa_cuti' => $get_sisa_cuti,
            'date_before' => $str_minus,
            'gaji' => $naik_gaji,
            'naik_pangkat' => $get_pangkat,
        ];
        return view('home.dashboard', $data);
    }
}
