<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class generateCuti extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate-cuti';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $get_user = DB::table('users')
                ->where('deleted_at',NULL)
                ->get();
        foreach($get_user as $gu){
            $get_sisa_cuti = DB::table('tabel_sisa_cuti')
                        ->where('kode_user',$gu->kode_user)
                        ->get();
            $sisa_cuti = array();
            if(count($get_sisa_cuti) > 0){
                foreach($get_sisa_cuti as $gs){
                    $sisa_cuti[] = $gs->sisa_cuti;
                }
            }else{
                $sisa_cuti;
            }
            $array_sum = array_sum($sisa_cuti) + 12;
            // $insert
            $cek_insert = DB::table('tabel_sisa_cuti')
                        ->where([
                            ['kode_user',$gu->kode_user],
                            ['tahun',date('Y')]
                        ]);
            if(!$cek_insert){
                DB::table('tabel_sisa_cuti')
                    ->insert([
                        'kode_user' => $gu->kode_user,
                        'tahun_cuti' => date('Y'),
                        'sisa_cuti' => $array_sum,
                        'created_at' => now(),
                    ]);
                echo 'generate cuti'."\n";
            }else{
                echo 'Sudah di generate'."\n";
            }

        }
    }
}
