<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Mail;

class generate_pangkat extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate_pangkat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $get_riwayat = DB::table('tabel_riwayat_pangkat')
                    ->where('deleted_at',NULL)
                    ->latest()
                    ->get();
        $user = array();
        foreach($get_riwayat as $gr){
            $get_pendidikan = DB::table('users')
                            ->where('kode_user',$gr->kode_user)
                            ->first();
            $get_id_pangkat = $gr->pangkat_id +1;
            $getPangkat = DB::table('tabel_pangkat_master')
                        ->where('id', $get_id_pangkat)
                        ->get();
            foreach($getPangkat as $gp){
                $pendidikan = explode(',',$gp->pendidikan);
            }

            $get_latest = DB::table('tabel_riwayat_pangkat')
                    ->where([
                        ['kode_user',$gr->kode_user],
                        ['deleted_at',NULL],
                    ])
                    ->latest()
                    ->first();
            if(in_array($get_pendidikan->pendidikan_terakhir, $pendidikan)){
                $tanggal_tmt = $get_latest->tmt_selanjutnya;
                $additionMinusOne = date('Y-m-d', strtotime('-1 month', strtotime($tanggal_tmt)));

                $tanggal_sekarang = date('Y-m-d');
                $new_date = date('Y-m-d', strtotime('+4 years', strtotime($tanggal_tmt)));
                $get_user = DB::table('users')
                            ->where([
                                ['kode_user',$gr->kode_user],
                            ])
                            ->first();
                if($additionMinusOne == $tanggal_sekarang){
                    $user[] = $gr->kode_user;
                    // kirim email ke pegawai
                    $data = [
                        'pesan_pegawai' => 'Kenaikan pangkat anda anda akan naik pada tanggal '.$tanggal_tmt.' Silahkan siapkan berkas-berkas yang diperlukan',
                        'user' => $get_user,
                        'judul' => 'Reminder Kenaikan Pangkat',
                    ];
                    Mail::send('mail_template.mail_pegawai', $data, function ($message) use ($get_user)
                    {
                        $message->subject('Info Kenaikan Pangkat');
                        $message->from('rex.three110@gmail.com', 'Notifikasi Kenaikan Gaji');
                        $message->to($get_user->email_pribadi);
                    });
                }else if($tanggal_tmt == $tanggal_sekarang){
                    $insert = DB::table('tabel_riwayat_pangkat')
                            ->insert([
                                'kode_user' => $gr->kode_user,
                                'pangkat_id' => $get_id_pangkat,
                                'tmt' => $gr->tmt_selanjutnya,
                                'tmt_selanjutnya' => $new_date,
                                'created_at' => now(),
                            ]);
                        echo 'insert data generate pangkat'."\n";
                }else{
                    echo 'tidak ada yang di generate'."\n";
                }
            }
        }
        if(count($user) > 0){
            // kirim email ke admin
            $get_admin = DB::table('users')
                    ->whereIn('id_level',[1,2])
                    ->get();
            foreach($get_admin as $ga){
                $data = [
                    'nama' => $ga->nama_lengkap,
                    'judul' => 'Reminder Kenaikan Pangkat',
                    'list' => $user,
                    'tanggal' => $tanggal_tmt,
                    'pesan' => 'Berikut pegawai yang akan naik pangkat pada tanggal '.$tanggal_tmt,
                ];
                Mail::send('mail_template.mail_admin', $data, function ($message) use ($ga)
                {
                    $message->subject('Info Kenaikan Pangkat');
                    $message->from('rex.three110@gmail.com', 'Notifikasi Kenaikan Gaji');
                    $message->to($ga->email_pribadi);
                });
            }
            echo 'kirim email ke semua admin'."\n";
        }
    }
}
