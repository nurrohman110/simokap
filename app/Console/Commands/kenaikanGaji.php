<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Mail;

class kenaikanGaji extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kenaikanGaji';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $get_kenaikan_gaji = DB::table('tabel_schedule_naik_gaji')
                    ->where('deleted_at',NULL)
                    ->latest()
                    ->get();
        if(count($get_kenaikan_gaji) > 0){
            $user=  array();
            foreach($get_kenaikan_gaji as $gk){
                $get_latest = DB::table('tabel_schedule_naik_gaji')
                            ->where([
                                ['kode_user',$gk->kode_user],
                                ['deleted_at',NULL]
                            ])
                            ->latest()
                            ->first();
                $get_user = DB::table('users')
                            ->where([
                                ['kode_user',$gk->kode_user],
                            ])
                            ->first();
                $dateNow = date('Y-m-d');
                $cek_tanggal = $get_latest->tanggal;
                $additionOneMonth = date('Y-m-d', strtotime('-1 month', strtotime($cek_tanggal)));
                $new_date = date('Y-m-d', strtotime('+2 years', strtotime($cek_tanggal)));


                if($additionOneMonth == $dateNow){
                    $user[] = $gk->kode_user;

                    // untuk mengirim email ke pegawai
                    $data = [
                        'pesan_pegawai' => 'Kenaikan gaji anda akan naik pada tanggal '.$cek_tanggal.' Silahkan siapkan berkas-berkas yang diperlukan',
                        'user' => $get_user,
                        'judul' => 'Reminder Kenaikan Gaji',
                    ];
                    Mail::send('mail_template.mail_pegawai', $data, function ($message) use ($get_user)
                    {
                        $message->subject('Info Kenaikan Gaji');
                        $message->from('rex.three110@gmail.com', 'Notifikasi Kenaikan Gaji');
                        $message->to($get_user->email_pribadi);
                    });
                    echo 'kirim email ke pegawai yang bersangkutan'."\n";
                }else if($cek_tanggal == $dateNow){
                    $insert = DB::table('tabel_schedule_naik_gaji')
                            ->insert([
                                'kode_user' => $gk->kode_user,
                                'tanggal' => $new_date,
                                'created_at' => now(),
                            ]);
                    echo 'insert data jadwal naik gaji'."\n";
                }else{
                    echo "tidak ada "."\n";
                }
            }
            // untuk mengirim email ke admin
            $get_admin = DB::table('users')
                    ->whereIn('id_level',[1,2])
                    ->get();
            foreach($get_admin as $ga){
                $data = [
                    'nama' => $ga->nama_lengkap,
                    'judul' => 'Reminder Kenaikan Gaji',
                    'list' => $user,
                    'tanggal' => $cek_tanggal,
                    'pesan' => 'Berikut pegawai yang akan naik gaji pada tanggal '.$cek_tanggal,
                ];
                Mail::send('mail_template.mail_admin', $data, function ($message) use ($ga)
                {
                    $message->subject('Info Kenaikan Gaji');
                    // $message->from('testing@oteem.id', 'Notifikasi Kenaikan Gaji');
                    $message->from('rex.three110@gmail.com', 'Notifikasi Kenaikan Gaji');
                    $message->to($ga->email_pribadi);
                });
            }
            echo 'kirim email ke semua admin'."\n";
        }else{
            echo 'data belum ada';
        }
    }
}
