<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use DB;
use Illuminate\Support\Facades\Blade;
use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::if('role', function ($expression) {
            $get_level = DB::table('tabel_user_level')->where([
                           ['name',$expression],
                           ['id',Auth::user()->id_level]
                         ])->first();
            $condition = isset($get_level);
            return $condition;
          });
    }
}
