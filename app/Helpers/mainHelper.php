<?php

function tanggal($input,$getTime = true)
{
    if (strlen($input) > 0) {
        if (strlen($input) > 10) {
            $get_date = substr($input,0,10);
            $time = ltrim($input,$get_date);
        } else {
            $get_date = $input;
            $time = '';
        }

        $input = explode('-',$get_date);
        $tanggal = $input[2];
        $bulan = $input[1];
        $tahun = $input[0];
        if($tahun < 100){
            $tahun = "20".$tahun;
        }
        if($bulan == "01"){
            $bulan = "Januari";
        }elseif($bulan == "02"){
            $bulan = "Februari";
        }elseif($bulan == "03"){
            $bulan = "Maret";
        }elseif($bulan == "04"){
            $bulan = "April";
        }elseif($bulan == "05"){
            $bulan = "Mei";
        }elseif($bulan == "06"){
            $bulan = "Juni";
        }elseif($bulan == "07"){
            $bulan = "Juli";
        }elseif($bulan == "08"){
            $bulan = "Agustus";
        }elseif($bulan == "09"){
            $bulan = "September";
        }elseif($bulan == "10"){
            $bulan = "Oktober";
        }elseif($bulan == "11"){
            $bulan = "November";
        }elseif($bulan == "12"){
            $bulan = "Desember";
        }
        if ($getTime == true) {
            $hasil = $tanggal." ".$bulan." ".$tahun.$time;
        } else {
            $hasil = $tanggal." ".$bulan." ".$tahun;
        }
        return $hasil;
    } else {
        return '-';
    }
}

function show_modul(){
    $role_id = Auth::user()->id_level;
    $get_modul_akses = DB::table('tabel_modul_akses')
                        ->where([
                            ['id_level',$role_id],
                            ['deleted_at',NULL]
                        ])
                        ->orderBy('id','asc')
                        ->get();
    if (count($get_modul_akses) > 0) {
        $modul = array();
        foreach ($get_modul_akses as $gma) {
            $get_modul = DB::table('tabel_modul')
                        ->where([
                            ['id',$gma->id_modul],
                            ['deleted_at',NULL]
                        ])
                        ->first();
            if (isset($get_modul) > 0) {
                $modul[] = $get_modul;
            }
        }
    }

    $step = 1;
    $show_modules = '';
    foreach ($modul as $mdl) {
        $base_url = substr(url()->current(),strlen(url('/')));
        $parse_url = explode('/',$base_url);
        $shift_array = array_shift($parse_url);

        if (count($parse_url) > 0) {
            $primary_url = $parse_url[0];
        } else {
            $primary_url = '/';
        }

        if (url($primary_url) == url($mdl->url)) {
            $show_modules = $show_modules.'
                <li class="active">
                    <a href="'.url($mdl->url).'" class="has-arrow" aria-expanded="false">
                        <i class="'.$mdl->icon.'"></i>
                        <span class="hide-menu">'.$mdl->nama_modul.'
                    </a>
                </li>';
        } else {
                $show_modules = $show_modules.'
                    <li class="">
                        <a href="'.url($mdl->url).'" class="has-arrow" aria-expanded="false">
                            <i class="'.$mdl->icon.'"></i>
                            <span class="hide-menu">'.$mdl->nama_modul.'
                        </a>
                    </li>';
        }
    }
    echo $show_modules;
}
function level($id){
    $level = DB::table('tabel_user_level')
            ->where('id',$id)
            ->first();
    if(isset($level)){
        return $level->name;
    }else{
        return '-';
    }
}
function modul($id){
    $modul = DB::table("tabel_modul")
            ->where([
                ['id',$id],
                ['deleted_at',NULL]
            ])
            ->first();
    if(isset($modul)){
        return $modul->nama_modul;
    }else{
        return '-';
    }
}

function pangkat($id){
    $pangkat = DB::table('tabel_pangkat_master')
            ->where([
                ['id',$id],
                ['deleted_at',NULL],
            ])
            ->first();
    if(isset($pangkat)){
        return $pangkat->nama_pangkat;
    }else{
        return '-';
    }
}

function nama_lengkap($kode_user){
    $nama = DB::table('users')
            ->where('kode_user',$kode_user)
            ->first();
    if(isset($nama)){
        return $nama->nama_lengkap;
    }else{
        return '-';
    }
}

function loopingYears(){
    $firstYear = (int)date('Y') - 20;
    $lastYear = $firstYear + 40;

    for($i=$firstYear;$i<=$lastYear;$i++)
    {
        echo '<option value='.$i.'>'.$i.'</option>';
    }
}
