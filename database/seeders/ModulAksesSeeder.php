<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class ModulAksesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tabel_modul_akses')->truncate();
        DB::table('tabel_modul_akses')
            ->insert([
                [
                    'id' => 1,
                    'id_level' => 1,
                    'id_modul' => 1,
                    'created_at' => now(),
                ],
                [
                    'id' => 2,
                    'id_level' => 1,
                    'id_modul' => 2,
                    'created_at' => now(),
                ],
                [
                    'id' => 3,
                    'id_level' => 1,
                    'id_modul' => 3,
                    'created_at' => now(),
                ],
                [
                    'id' => 4,
                    'id_level' => 1,
                    'id_modul' => 4,
                    'created_at' => now(),
                ],
                [
                    'id' => 5,
                    'id_level' => 1,
                    'id_modul' => 5,
                    'created_at' => now(),
                ],
                [
                    'id' => 6,
                    'id_level' => 1,
                    'id_modul' => 6,
                    'created_at' => now(),
                ],
                [
                    'id' => 7,
                    'id_level' => 1,
                    'id_modul' => 7,
                    'created_at' => now(),
                ],
                // [
                //     'id' => 8,
                //     'id_level' => 1,
                //     'id_modul' => 8,
                //     'created_at' => now(),
                // ],
                [
                    'id' => 9,
                    'id_level' => 1,
                    'id_modul' => 9,
                    'created_at' => now(),
                ],
                [
                    'id' => 10,
                    'id_level' => 1,
                    'id_modul' => 10,
                    'created_at' => now(),
                ],
                [
                    'id' => 11,
                    'id_level' => 1,
                    'id_modul' => 11,
                    'created_at' => now(),
                ],
                [
                    'id' => 12,
                    'id_level' => 1,
                    'id_modul' => 12,
                    'created_at' => now(),
                ],

            ]);
    }
}
