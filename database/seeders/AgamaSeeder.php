<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class AgamaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tabel_agama')->truncate();
        DB::table('tabel_agama')
            ->insert([
                [
                    'id' => 1,
                    'nama_agama' => 'Islam',
                    'created_at' => now(),
                ],
                [
                    'id' => 2,
                    'nama_agama' => 'Protestan',
                    'created_at' => now(),
                ],
                [
                    'id' => 3,
                    'nama_agama' => 'Katolik',
                    'created_at' => now(),
                ],
                [
                    'id' => 4,
                    'nama_agama' => 'Hindu',
                    'created_at' => now(),
                ],
                [
                    'id' => 5,
                    'nama_agama' => 'Buddha',
                    'created_at' => now()
                ],
                [
                    'id' => 6,
                    'nama_agama' => 'Khonghucu',
                    'created_at' => now(),
                ],
            ]);
    }
}
