<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class JenisPegawaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tabel_jenis_pegawai')->truncate();
        DB::table('tabel_jenis_pegawai')
            ->insert([
                [
                    'id' => 1,
                    'nama_jenis' => 'PNS',
                    'created_at' => now(),
                ],
                [
                    'id' => 2,
                    'nama_jenis' => 'CPNS',
                    'created_at' => now(),
                ],
                [
                    'id' => 3,
                    'nama_jenis' => 'PPNPN',
                    'created_at' => now(),
                ]
            ]);
    }
}
