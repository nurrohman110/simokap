<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class ModulSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tabel_modul')->truncate();
        DB::table('tabel_modul')
            ->insert([
                [
                    'id' => 1,
                    'nama_modul' => 'Dashboard',
                    'icon' => 'fa fa-dashboard',
                    'url' => 'home',
                    'created_at' => now(),
                ],
                [
                    'id' => 2,
                    'nama_modul' => 'Aktifias Harian',
                    'icon' => 'fa fa-calendar-o',
                    'url' => 'activity-harian',
                    'created_at' => now(),
                ],
                [
                    'id' => 3,
                    'nama_modul' => 'Pengajuan Cuti',
                    'icon' => 'fa fa-envelope',
                    'url' => 'pengajuan-cuti',
                    'created_at' => now(),
                ],
                [
                    'id' => 4,
                    'nama_modul' => 'Riwayat Kepangkatan',
                    'icon' => 'fa fa-bookmark',
                    'url' => 'riwayat-pangkat',
                    'created_at' => now(),
                ],
                [
                    'id' => 5,
                    'nama_modul' => 'Sisa Cuti Pegawai',
                    'icon' => 'fa fa-database',
                    'url' => 'sisa-cuti',
                    'created_at' => now(),
                ],
                [
                    'id' => 5,
                    'nama_modul' => 'Pangkat Master',
                    'icon' => 'fa fa-cubes',
                    'url' => 'pangkat',
                    'created_at' => now(),
                ],
                [
                    'id' => 6,
                    'nama_modul' => 'Pegawai',
                    'icon' => 'fa fa-users',
                    'url' => 'pegawai',
                    'created_at' => now(),
                ],
                [
                    'id' => 7,
                    'nama_modul' => 'User Level',
                    'icon' => 'fa fa-child',
                    'url' => 'user-level',
                    'created_at' => now(),
                ],
                // [
                //     'id' => 8,
                //     'nama_modul' => 'Type Cuti',
                //     'icon' => 'fa fa-tasks',
                //     'url' => 'type-cuti',
                //     'created_at' => now(),
                // ],
                [
                    'id' => 9,
                    'nama_modul' => 'Agama',
                    'icon' => 'fa fa-shield',
                    'url' => 'agama',
                    'created_at' => now(),
                ],
                [
                    'id' => 10,
                    'nama_modul' => 'Modul Akses',
                    'icon' => 'fa fa-cog',
                    'url' => 'modul',
                    'created_at' => now(),
                ],
                [
                    'id' => 11,
                    'nama_modul' => 'Management Access',
                    'icon' => 'fa fa-unlock-alt',
                    'url' => 'management-access',
                    'created_at' => now(),
                ],
                [
                    'id' => 12,
                    'nama_modul' => 'Kenaikan Gaji',
                    'icon' => 'fa fa-list-ul',
                    'url' => 'kenaikan-gaji',
                    'created_at' => now(),
                ]
            ]);
    }
}
