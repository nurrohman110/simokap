<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class UserTabelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        DB::table('users')
            ->insert([
                [
                    'id' => 1,
                    'name' => 'admin',
                    'username' => 'admin1',
                    'kode_user' => 'KD1',
                    'nama_lengkap' => 'Admin 123',
                    'nip' => '123',
                    'image' => '',
                    'email' => 'admin@simokap.com',
                    'password' => bcrypt(123456),
                    'secret' => '123456',
                    'id_level' => 1,
                    'jenis_kelamin' => 'L',
                    'tempat_lahir' => 'Demak',
                    'tanggal_lahir' => '2020-02-20',
                    'agama_id' => '1',
                    'created_at' => now(),
                ],
            ]);
    }
}
