<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class UserLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("tabel_user_level")->truncate();
        DB::table('tabel_user_level')
            ->insert([
                [
                    'id' => 1,
                    'name' => 'SuperAdmin',
                    'created_at' => now(),
                ],
                [
                    'id' => 2,
                    'name' => 'Admin',
                    'created_at' => now(),
                ],
                [
                    'id' => 3,
                    'name' => 'User',
                    'created_at' => now(),
                ]
            ]);
    }
}
