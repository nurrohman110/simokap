<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTabelActivityHarian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabel_activity_harian', function (Blueprint $table) {
            $table->id();
            $table->string('kode_user');
            $table->date('tanggal');
            $table->string('kode_activity')->unique();
            $table->text('description')->nullable();
            $table->string('parent_kode_activity')->nullable();
            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabel_activity_harian');
    }
}
