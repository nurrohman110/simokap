<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTabelJabatanPegawai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabel_riwayat_pangkat', function (Blueprint $table) {
            $table->id();
            $table->string('pangkat_id');
            $table->string('kode_user');
            $table->string('tmt');
            $table->string('tmt_selanjutnya')->nullable();
            $table->string('keterangan');
            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabel_jabatan_pegawai');
    }
}
