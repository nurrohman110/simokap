<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTabelPengajuanCuti extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabel_pengajuan_cuti', function (Blueprint $table) {
            $table->id();
            $table->string('kode_cuti')->unique();
            $table->string('kode_user');
            $table->date('tanggal_cuti')->nullable();
            $table->date('tanggal_selesai_cuti')->nullable();
            $table->string('jumlah_hari_cuti')->nullable();
            $table->string('alasan_cuti')->nullable();
            $table->string('no_handphone')->nullable();
            $table->string('alamat_cuti')->nullable();
            $table->string('description')->nullable();
            $table->integer('jenis_cuti_id')->nullable();
            $table->timestamp('read_at')->nullable();
            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabel_pengajuan_cuti');
    }
}
