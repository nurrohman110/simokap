<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('nama_panggilan');
            $table->string('username')->unique()->nullable();
            $table->string('kode_user')->unique();
            $table->string('nama_lengkap');
            $table->string('nip')->unique();
            $table->string('email_pribadi')->unique()->nullable();
            $table->string('email_kantor')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->date('tanggal_gabung')->nullable();
            $table->string('password');
            $table->string('secret');
            $table->integer('id_level');
            $table->string('image')->nullable();
            $table->string('jenis_kelamin')->nullable();
            $table->string('tempat_lahir')->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->string('agama_id')->nullable();
            $table->string('status_perkawinan')->nullable();
            $table->string('alamat_lama')->nullable();
            $table->string('alamat_baru')->nullable();
            $table->integer('jenis_pegawai_id')->nullable();
            $table->string('no_telepon')->nullable();
            $table->string('npwp')->nullable();
            $table->string('pendidikan_terakhir')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
