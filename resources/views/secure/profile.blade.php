@extends('layout.template')
@section('title')
    Profile
@endsection

@push('style')
    <style>
        li {
            list-style: none
          }

          .hide-password {
            display: none
          }
    </style>
@endpush
@section('content')
<div class="mainpanel">
    <div class="pageheader">
        <div class="media">
            <div class="pageicon pull-left">
                <i class="fa fa-user"></i>
            </div>
            <div class="media-body">
                <ul class="breadcrumb">
                    <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                    <li>Halaman Profile</li>
                </ul>
                <h4>Halaman Profile</h4>
            </div>
        </div><!-- media -->
    </div><!-- pageheader -->

    <div class="contentpanel">
        @if (session('success'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Well done!</strong> Berhasil memperbarui profile</a>.
            </div>
        @endif

        @if (session('gagal'))
            <div class="alert alert-warning">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Warning!</strong> Gagal memperbarui profile
            </div>
        @endif

        @if (session('info'))
            <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Heads up!</strong> This <a href="" class="alert-link">alert needs your attention</a>, but it not super important.
            </div>
        @endif

        <div class="row">
            <div class="col-sm-4 col-md-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="text-center">
                            {{--  <img src="{{ asset('public/asset/images/photos/profile-big.jpg') }}" class="img-circle img-offline img-responsive img-profile" alt="" />  --}}
                            @if (Auth::user()->image != null)
                            <img src="{{ URL::to('/') }}{{ Auth::user()->image }}"  class="img-circle img-offline img-responsive img-profile" style="border-radius: 50%; width: 200px;" alt="Avatar">
                            @else
                            <img src="https://image.flaticon.com/icons/png/512/17/17004.png"  class="img-circle img-offline img-responsive img-profile" style="border-radius: 50%; width: 200px;" alt="Avatar">
                            @endif
                            <h4 class="profile-name mb5">{{ Auth::user()->nama_lengkap }}</h4>
                            <div><i class="fa fa-envelope"></i> {{ Auth::user()->email_kantor }}</div>
                            <div><i class="fa fa-map-marker"></i> {{ Auth::user()->alamat_baru }}</div>
                            <div class="mb20"></div>
                        </div><!-- text-center -->
                    </div>
                </div>

            </div>
            <div class="col-sm-8 col-md-9">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs nav-line">
                            <li class="active"><a href="#activities" data-toggle="tab"><strong>Timeline</strong></a></li>
                            <li><a href="#following" data-toggle="tab"><strong>Setting</strong></a></li>
                            <li><a href="#change_image" data-toggle="tab"><strong>Ganti Foto Profil</strong></a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content nopadding noborder">
                            <div class="tab-pane active" id="activities">
                                <div class="activity-list">
                                    <h4 class="panel-title"><span class="text-info">Nama Lengkap</span></h4>
                                    <strong>{{ Auth::user()->nama_lengkap }}</strong>
                                    <br><br>
                                    <h4 class="panel-title"><span class="text-info">NIP</span></h4>
                                    <strong>{{ Auth::user()->nip }}</strong>
                                    <br><br>
                                    <h4 class="panel-title"><span class="text-info">Email Pribadi</span></h4>
                                    <strong>{{ Auth::user()->email_pribadi }}</strong>
                                    <br><br>
                                    <h4 class="panel-title"><span class="text-info">Email Kantor</span></h4>
                                    <strong>{{ Auth::user()->email_kantor }}</strong>
                                    <br><br>
                                    <h4 class="panel-title"><span class="text-info">Tempat, Tanggal Lahir</span></h4>
                                    <strong>{{ Auth::user()->tempat_lahir }}, {{ Auth::user()->tanggal_lahir }}</strong>
                                    <br><br>
                                    <h4 class="panel-title"><span class="text-info">Jenis Kelamin</span></h4>
                                    <strong>{{ Auth::user()->jenis_kelamin == 'L' ? 'Laki - Laki' : 'Perempuan' }}</strong>
                                    <br><br>
                                    <h4 class="panel-title"><span class="text-info">Alamat Lama</span></h4>
                                    <strong>{{ Auth::user()->alamat_lama }}</strong>
                                    <br><br>
                                    <h4 class="panel-title"><span class="text-info">Alamat Baru</span></h4>
                                    <strong>{{ Auth::user()->alamat_baru ?? '-' }}</strong>
                                    <br><br>
                                    <h4 class="panel-title"><span class="text-info">Nomor Telepon</span></h4>
                                    <strong>{{ Auth::user()->no_telepon }}</strong>
                                    <br><br>
                                </div>
                            </div>

                            <div class="tab-pane" id="following">
                                <form action="{{ url('update_profile') }}" method="POST">
                                    @csrf
                                    <div class="panel-body">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label class="control-label"><h5><span class="text-info"> Nama Lengkap</span></h5></label>
                                                <input type="text" name="nama_lengkap" value="{{ Auth::user()->nama_lengkap }}" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label class="control-label"><h5><span class="text-info"> Email Pribadi</span></h5></label>
                                                <input type="email" name="email_pribadi" class="form-control" value="{{ Auth::user()->email_pribadi }}" />
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label class="control-label"><h5><span class="text-info"> Email Kantor</span></h5></label>
                                                <input type="email" name="email_kantor" class="form-control" value="{{ Auth::user()->email_kantor }}"  />
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label class="control-label"><h5><span class="text-info">Tempat Lahir</span></h5></label>
                                                <input type="text" name="tempat_lahir" class="form-control" value="{{ Auth::user()->tempat_lahir }}" required />
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label class="control-label"><h5><span class="text-info">Tanggal Lahir</span></h5></label>
                                                <input type="date" name="tanggal_lahir" id="tanggal_lahir" class="form-control" value="{{ Auth::user()->tanggal_lahir }}" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label class="control-label"><h5><span class="text-info">Password</span></h5></label>
                                                <input type="password" name="password" id="password" class="form-control" value="{{ Auth::user()->secret }}" required>
                                                <span class="password-showhide">
                                                <span class="show-password">Show</span>
                                                <span class="hide-password">hide</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label class="control-label"><h5><span class="text-info">Alamat Lama</span></h5></label>
                                                <input type="text" name="alamat_lama" id="alamat_lama" value="{{ Auth::user()->alamat_lama }}" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label class="control-label"><h5><span class="text-info">Alamat Baru</span></h5></label>
                                                <input type="text" name="alamat_baru" id="alamat_baru" class="form-control" value="{{ Auth::user()->alamat_baru }}" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label class="control-label"><h5><span class="text-info">Nomor Telepon</span></h5></label>
                                                <input type="number" min="0" name="nomor_telepon" value="{{ Auth::user()->no_telepon }}" id="nomor_telepon" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <button class="btn btn-primary"><span class="text-center"> Simpan</span></button>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="change_image">
                                <form action="{{ url('/updateFoto') }}"  enctype="multipart/form-data" method="POST">
                                    @csrf
                                    <div class="panel-body">
                                        <div class="center">
                                            @if (Auth::user()->image != null)
                                            <img src="{{ URL::to('/') }}{{ Auth::user()->image }}" style="border-radius: 50%; width: 200px;" alt="Avatar">
                                            @else
                                            <img src="https://image.flaticon.com/icons/png/512/17/17004.png" style="border-radius: 50%; width: 200px;" alt="Avatar">
                                            @endif
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label class="control-label"><h5><span class="text-info">Foto Profil</span></h5></label>
                                                <input type="file" name="image_profile" id="image_profile" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <button class="btn btn-primary"><span class="text-center"> Simpan</span></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- tab-content -->

            </div><!-- col-sm-9 -->
        </div><!-- row -->

    </div><!-- contentpanel -->
</div>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            $(".show-password, .hide-password").on('click', function() {
                if ($(this).hasClass('show-password')) {
                    $("#password").attr("type", "text");
                    $(this).parent().find(".show-password").hide();
                    $(this).parent().find(".hide-password").show();
                    console.log('show');
                } else {
                    $("#password").attr("type", "password");
                    $(this).parent().find(".hide-password").hide();
                    $(this).parent().find(".show-password").show();
                    console.log('hide');
                }
            });
        });
    </script>
@endpush
