<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Login</title>
        <link href="{{ asset('public/asset/css/style.default.css') }}" rel="stylesheet">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="{{ asset('public/asset/js/html5shiv.js') }}"></script>
        <script src="{{ asset('public/asset/js/respond.min.js') }}"></script>
        <![endif]-->
    </head>
    <body class="signin">
        <section>
            <div class="panel panel-signin">
                @if (session('salah'))
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>{{ session('salah') }}</strong>
                    </div>
                @endif
                <div class="panel-body">
                    <div class="logo text-center">
                        <strong style="color: blue; font-size: 180%;" >SIMOKAP</strong>
                    </div>
                    <br/>
                    <p class="text-center">Login Simokap</p>
                    <div class="mb30"></div>
                    <form action="{{ url('postLogin') }}" method="post">
                        @csrf
                        <div class="input-group mb15">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input type="number" min="0" class="form-control" name="nip" id="nip" placeholder="NIP">
                        </div><!-- input-group -->
                        <div class="input-group mb15">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                        </div><!-- input-group -->

                        <div class="clearfix">
                            <div class="pull-right">
                                <button type="submit" class="btn btn-success">Sign In <i class="fa fa-angle-right ml5"></i></button>
                            </div>
                        </div>
                    </form>

                </div><!-- panel-body -->
            </div><!-- panel -->
        </section>
        <script src="{{ asset('public/asset/js/jquery-1.11.1.min.js') }}"></script>
        <script src="{{ asset('public/asset/js/jquery-migrate-1.2.1.min.js') }}"></script>
        <script src="{{ asset('public/asset/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('public/asset/js/modernizr.min.js') }}"></script>
        <script src="{{ asset('public/asset/js/pace.min.js') }}"></script>
        <script src="{{ asset('public/asset/js/retina.min.js') }}"></script>
        <script src="{{ asset('public/asset/js/jquery.cookies.js') }}"></script>
        <script src="{{ asset('public/asset/js/custom.js') }}"></script>
    </body>
</html>
