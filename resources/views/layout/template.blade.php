<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Simokap - @yield('title')</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="{{ asset('public/asset/css/style.default.css') }}" rel="stylesheet">
        <link href="{{ asset('public/asset/css/morris.css') }}" rel="stylesheet">
        <link href="{{ asset('public/asset/css/select2.css') }}" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="{{ asset('public/asset/js/html5shiv.js') }}"></script>
        <script src="{{ asset('public/asset/js/respond.min.js') }}"></script>
        <![endif]-->
        <link href="{{ asset('public/asset/css/style.datatables.css') }}" rel="stylesheet">
        <link href="http://cdn.datatables.net/responsive/1.0.1/css/dataTables.responsive.css" rel="stylesheet">
        @stack('style')
    </head>
    <body>
        <header>
            <div class="headerwrapper">
                <div class="header-left">
                    <a href="{{ url('/home') }}" class="logo">
                        {{--  <img src="{{ asset('public/asset/images/logo.png') }}" alt="" />  --}}
                        <strong style="color: white; font-size: 20px;">SIMOKAP</strong>
                    </a>
                    <div class="pull-right">
                        <a href="" class="menu-collapse">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                </div><!-- header-left -->
                <div class="header-right">
                    <div class="pull-right">

                        <div class="btn-group btn-group-option">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                              <i class="fa fa-caret-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right" role="menu">
                              <li><a href="{{ url('/profile') }}"><i class="glyphicon glyphicon-user"></i> My Profile</a></li>

                              <li class="divider"></li>
                              <li><a href="{{ url('/logout') }}"><i class="glyphicon glyphicon-log-out"></i>Sign Out</a></li>
                            </ul>
                        </div><!-- btn-group -->

                    </div><!-- pull-right -->

                </div><!-- header-right -->

            </div><!-- headerwrapper -->
        </header>

        <section>
            <div class="mainwrapper">
                <div class="leftpanel">
                    <div class="media profile-left">
                        <a class="pull-left profile-thumb" href="profile.html">
                            <img class="img-circle" src="images/photos/profile.png" alt="">
                            @if (Auth::user()->image != null)
                            <img src="{{ URL::to('/') }}{{ Auth::user()->image }}" class="img-circle"alt="Avatar">
                            @else
                            <img src="https://image.flaticon.com/icons/png/512/17/17004.png"  class="img-circle" alt="Avatar">
                            @endif
                        </a>
                        <div class="media-body">
                            <h4 class="media-heading">{{ Auth::user()->nama_lengkap }}</h4>
                            <small class="text-muted">{{ Auth::user()->email_kantor }}</small>
                        </div>
                    </div><!-- media -->

                    <h5 class="leftpanel-title">Navigation</h5>
                    <ul class="nav nav-pills nav-stacked">
                        {{ show_modul() }}
                    </ul>
                </div><!-- leftpanel -->
                @yield('content')
            </div><!-- mainwrapper -->
        </section>
    </body>
    {{--  <script src="{{ asset('public/asset/js/jquery-1.11.1.min.js') }}"></script>
    <script src="{{ asset('public/asset/js/jquery-migrate-1.2.1.min.js') }}"></script>  --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script src="{{ asset('public/asset/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/asset/js/modernizr.min.js') }}"></script>
    <script src="{{ asset('public/asset/js/pace.min.js') }}"></script>
    <script src="{{ asset('public/asset/js/retina.min.js') }}"></script>
    <script src="{{ asset('public/asset/js/jquery.cookies.js') }}"></script>

    <script src="{{ asset('public/asset/js/flot/jquery.flot.min.js') }}"></script>
    <script src="{{ asset('public/asset/js/flot/jquery.flot.resize.min.js') }}"></script>
    <script src="{{ asset('public/asset/js/flot/jquery.flot.spline.min.js') }}"></script>

    <script src="{{ asset('public/asset/js/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('public/asset/js/morris.min.js') }}"></script>
    <script src="{{ asset('public/asset/js/raphael-2.1.0.min.js') }}"></script>
    <script src="{{ asset('public/asset/js/bootstrap-wizard.min.js') }}"></script>
    <script src="{{ asset('public/asset/js/select2.min.js') }}"></script>
    <script src="{{ asset('public/asset/js/dashboard.js') }}"></script>


    {{--  datatable  --}}
    <script src="{{ asset('public/asset/js/jquery.dataTables.min.js') }}"></script>
    <script src="http://cdn.datatables.net/plug-ins/725b2a2115b/integration/bootstrap/3/dataTables.bootstrap.js"></script>
    <script src="http://cdn.datatables.net/responsive/1.0.1/js/dataTables.responsive.js"></script>

    {{--  js  --}}
    <script src="{{ asset('public/asset/js/custom.js') }}"></script>
    <script src="{{ asset('public/asset/js/sweetalert2.min.js') }}"></script>

    {{-- ckeditor --}}
    {{--  <script src="{{ asset('public/asset/js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('public/asset/js/ckeditor/adapters/jquery.js') }}"></script>  --}}
    {{-- <script src="{{ asset('public/asset/js/wysihtml5-0.3.0.min.js') }}"></script> --}}
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    @stack('js')
</html>
