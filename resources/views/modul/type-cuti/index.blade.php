@extends('layout.template')

@section('title')
    Jenis Cuti
@endsection

@section('content')
<div class="mainpanel">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <ul class="breadcrumb">
                    <li><a href="{{ url('/home') }}"><i class="glyphicon glyphicon-home"></i></a></li>
                    <li>Data Jenis Cuti</li>
                </ul>
                <h4>Data Jenis Cuti</h4>
            </div>
        </div><!-- media -->
    </div><!-- pageheader -->

    <div class="contentpanel">
        <div class="panel panel-primary-head">
            <div class="panel-heading">
                <h4 class="panel-title">Data Jenis Cuti</h4>
            </div><!-- panel-heading -->
            <br>
            {{-- <button class="btn btn-sm btn-primary" onclick="tambahData()"><span class="fa fa-plus"></span> Tambah</button> --}}
            <table id="sisa_cuti" class="table table-striped table-bordered responsive">
                <thead class="">
                    <tr>
                        <th>Nomor</th>
                        <th>Nama Lengkap</th>
                        <th>Tahun Cuti</th>
                        <th>Sisa Cuti</th>
                    </tr>
                </thead>
            </table>
        </div><!-- panel -->
    </div><!-- contentpanel -->
</div><!-- mainpanel -->
@endsection
