<div class="modal fade" id="modal_sisa_cuti" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <form class="form-horizontal">
                @csrf {{ method_field('POST') }}
                <div class="modal-header">
                    <h5 class="modal-title" id="sisa_cuti_title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="module_name" class="col-md-2 control-label">Pegawai <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <input type="hidden" id="id" value="">
                                <select name="kode_user" id="kode_user" class="form-control" required>
                                    <option value="">Pilih Pegawai</option>
                                    @foreach ($pegawai as $p)
                                        <option value="{{ $p->kode_user }}">{{ $p->nama_lengkap }}</option>
                                    @endforeach
                                </select>
                                <span class="help-block with-errors"></span>
                             </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="row">
                            <label for="module_name" class="col-md-2 control-label">Tahun <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <select name="tahun" id="tahun" class="form-control" required>
                                    <option value="">Pilih tahun</option>
                                    {{ loopingYears() }}
                                </select>
                                <span class="help-block with-errors"></span>
                             </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-2 control-label">Sisa Cuti</label>
                            <div class="col-md-8">
                                <input type="number" max="12" min="0" required class="form-control" name="sisa_cuti" id="sisa_cuti">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-close" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-save">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
