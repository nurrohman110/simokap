@extends('layout.template')

@section('title')
    Sisa Cuti
@endsection
@section('content')
<div class="mainpanel">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <ul class="breadcrumb">
                    <li><a href="{{ url('/home') }}"><i class="glyphicon glyphicon-home"></i></a></li>
                    <li>Data Sisa Cuti Pegawai</li>
                </ul>
                <h4>Data Sisa Cuti Pegawai</h4>
            </div>
        </div><!-- media -->
    </div><!-- pageheader -->

    <div class="contentpanel">
        <div class="panel panel-primary-head">
            <div class="panel-heading">
                <h4 class="panel-title">Data Sisa Cuti Pegawai</h4>
            </div><!-- panel-heading -->
            <br>
            <button class="btn btn-sm btn-primary" onclick="tambahData()"><span class="fa fa-plus"></span> Tambah</button>
            <table id="sisa_cuti" class="table table-striped table-bordered responsive">
                <thead class="">
                    <tr>
                        <th>Nomor</th>
                        <th>Nama Lengkap</th>
                        <th>Tahun Cuti</th>
                        <th>Sisa Cuti</th>
                    </tr>
                </thead>
            </table>
        </div><!-- panel -->
    </div><!-- contentpanel -->
</div><!-- mainpanel -->
@include('modul.sisa_cuti.modal')
@endsection

@push('js')
    <script>
        $('#kode_user, #tahun').select2();
        var sisa_cuti, save_method ;
        $(function () {
            sisa_cuti = $('#sisa_cuti').DataTable({
                ajax: '{{ url("data/sisa_cuti") }}',
                processing: true,
                serverside: true,
                bLengthChange: false,
                scrollXInner: true,
                dom: 'frtp'
            });
        });
        function tambahData(){
            save_method = 'add';
            $('#sisa_cuti_title').text('Tambah Sisa Cuti Pegawai');
            $('.btn-save').removeAttr('disabled');
            $('.btn-save').text('Save');
            $('#kode_user').val('').trigger('change');
            $('#tahun').val('').trigger('change');
            $('#modal_sisa_cuti form')[0].reset();
            $('#modal_sisa_cuti').modal('show');
        }

        $('.form-horizontal').on('submit', function(e){
            e.preventDefault();
            $('.btn-save').text('Saving . . . .');
            $('.btn-save').attr('disabled','disabled');

            if(save_method == 'add'){
                url = '{{ url("sisa_cuti/save") }}';
                message = 'Added';
            }else{

            }
            $.ajax({
                url : url,
                type : 'POST',
                data : $('.form-horizontal').serialize(),
                success: function(data){
                    if(data == false){
                        sisa_cuti.ajax.reload();
                        $('#modal_sisa_cuti').modal('hide');
                        Swal.fire(
                            message,
                            'Data yang anda masukkan sudah ada, silahkan cek ulang',
                            'info'
                        )
                    }else{
                        sisa_cuti.ajax.reload();
                        $('#modal_sisa_cuti').modal('hide');
                        Swal.fire(
                            message,
                            'Your data has been '+message,
                            'success'
                        )
                    }

                },
                error : function(){
                    $('#modal_sisa_cuti').modal('hide');
                    sisa_cuti.ajax.reload();
                    Swal.fire(
                        message,
                        'Your data has been '+msg,
                        'success'
                    )
                }
            })

        });
    </script>
@endpush
