<div class="modal fade" id="modal_pangkat" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <form class="form-horizontal">
                @csrf {{ method_field('POST') }}
                <div class="modal-header">
                    <h5 class="modal-title" id="title_modal"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="module_name" class="col-md-2 control-label">Nama Pangkat <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <input type="hidden" id="id" value="">
                                <input  type="text" id="nama_pangkat" class="form-control" name="nama_pangkat" required>
                                <span class="help-block with-errors"></span>
                             </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-2 control-label">Pendidikan <span class="text-danger"></span></label>
                            <div class="col-md-8">
                                <select name="pendidikan[]"  multiple id="pendidikan" class="form-control">
                                    <option value="SD">SD</option>
                                    <option value="SLTP">SLTP Sederajat</option>
                                    <option value="SLTA">SLTA Sederjat</option>
                                    <option value="DIII">DIII</option>
                                    <option value="S1">S1</option>
                                    <option value="S2">S2</option>
                                    <option value="S3">S3</option>
                                </select>
                            </div>
                        </div>
                     </div>
                     {{--  <div class="form-group">
                        <div class="row">
                            <label for="module_name" class="col-md-2 control-label">Keterangan <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="keterangan" name="keterangan">
                                <span class="help-block with-errors"></span>
                             </div>
                        </div>
                     </div>  --}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-close" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-save">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
