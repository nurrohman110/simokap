@extends('layout.template')
@section('title')
    Pangkat
@endsection

@section('content')
<div class="mainpanel">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <ul class="breadcrumb">
                    <li><a href="{{ url('/home') }}"><i class="glyphicon glyphicon-home"></i> Home</a></li>
                    <li>Data Pangkat</li>
                </ul>
                <h4>Data Pangkat</h4>
            </div>
        </div><!-- media -->
    </div><!-- pageheader -->

    <div class="contentpanel">
        <div class="panel panel-primary-head">
            <div class="panel-heading">
                <h4 class="panel-title">Data Pangkat</h4>
            </div><!-- panel-heading -->
            <br>
            <button class="btn btn-sm btn-primary" onclick="tambahData()"><span class="fa fa-plus"></span> Tambah</button>
            <table id="pangkat" class="table table-striped table-bordered responsive">
                <thead class="">
                    <tr>
                        <th>Nomor</th>
                        <th>Nama Pangkat</th>
                        <th>Pendidikan</th>
                        <th width="20%">Action</th>
                    </tr>
                </thead>
            </table>
        </div><!-- panel -->
    </div><!-- contentpanel -->
</div><!-- mainpanel -->
@include('modul.pangkat.modal')
@endsection

@push('js')
<script>
    var pangkat_tabel, save_method;
    $(function(){
        pangkat_tabel = $('#pangkat').DataTable({
            ajax: '{{ url("data/pangkat") }}',
            processing: true,
            serverside: true,
            LengthChange: false,
            scrollXInner: true,
            dom: 'lBfrtip'
        });
    });
    $('#pendidikan').select2();
    function tambahData(){
        save_method = 'add';
        $('#title_modal').text('Tambah Pangkat');
        $('#modal_pangkat form')[0].reset();
        $('.btn-save').removeAttr('disabled','disabled');
        $('#pendidikan').trigger('change');
        $('.btn-save').text('Save');
        $('#modal_pangkat').modal('show');
    }

    function updatePangkat(id){
        save_method = 'edit';
        $('#id').val(id);
        $('#title_modal').text('Update Pangkat');
        $('.btn-save').removeAttr('disabled','disabled');
        $('.btn-save').text('Update');
        $.ajax({
            url : '{{ url("/pangkat/gettingPangkat") }}/'+id,
            type : "GET",
            success : function(data){
                console.log(data);
                $("#nama_pangkat").val(data.nama_pangkat);
                $('#keterangan').val(data.keterangan);
                $('#pendidikan').select2().select2('val', data.testing[0]);
                $('#modal_pangkat').modal('show');
            },
            error : function(data){
                Swal.fire(
                    'Error Input',
                    'Gagal mengambil data',
                    'error',
                )
            }
        })
    }

    function deletePangkat(id){
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            console.log(result.isConfirmed);
            if (result.isConfirmed) {
                $.ajax({
                    url : '{{ url("/pangkat/hapus") }}/'+id,
                    type: 'POST',
                    data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
                    success: function(data){
                        Swal.fire(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        ),
                        pangkat_tabel.ajax.reload();
                    },
                    error: function(){
                        Swal.fire({
                            icon: 'success',
                            title: 'Data berhasil di hapus',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    }
                });
            }else{
                Swal.fire(
                    'Cancelled',
                    'Delete di cancel :)',
                    'error'
                )
            }
        })
    }

    $('.form-horizontal').on('submit', function(e){
        e.preventDefault();
        $('.btn-save').attr('disabled');
        $('.btn-save').text('Saving . . . .');
        var id = $('#id').val();
        if(save_method == 'add'){
            url = '{{ url("pangkat/save") }}';
            message = 'tambah';
        }else{
            url = '{{ url("pangkat/updatePangkat") }}/'+id;
            message = 'edit';
        }

        $.ajax({
            url : url,
            type : 'POST',
            data : $('.form-horizontal').serialize(),
            success: function(data){
                $('#modal_pangkat').modal('hide');
                pangkat_tabel.ajax.reload();
                Swal.fire({
                    icon: 'success',
                    title: 'Data berhasil di'+message,
                    showConfirmButton: false,
                    timer: 1500
                })
            },
            error: function(){
                Swal.fire({
                    icon: 'error',
                    title: 'Data gagal dihapus',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        });
    })
</script>
@endpush
