<div class="modal fade" id="modal_riwayat" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <form class="form-horizontal">
                @csrf {{ method_field('POST') }}
                <div class="modal-header">
                    <h5 class="modal-title" id="title_riwayat"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="module_name" class="col-md-2 control-label">Nama Pegawai <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <input type="hidden" id="id" value="">
                                <select name="kode_user" id="kode_user" class="form-control" required>
                                    <option value="">Pilih Pegawai</option>
                                    @foreach ($pegawai as $p)
                                        <option value="{{ $p->kode_user }}">{{ $p->nama_lengkap }}</option>
                                    @endforeach
                                </select>
                                <span class="help-block with-errors"></span>
                             </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="module_name" class="col-md-2 control-label">Pangkat Terakhir <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <select name="pangkat_id" class="form-control" id="pangkat_id" required>
                                    <option value="">Pilih Pangkat</option>
                                    @foreach ($pangkat as $p)
                                        <option value="{{ $p->id }}">{{ $p->nama_pangkat }}</option>
                                    @endforeach
                                </select>
                                <span class="help-block with-errors"></span>
                             </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-2 control-label">Tanggal TMT <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <input type="date" class="form-control" required name="tmt" id="tmt">
                            </div>
                        </div>
                    </div>
                    {{-- <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-2 control-label">Keterangan <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <input type="keterangan" class="form-control" name="keterangan" id="keterangan">
                            </div>
                        </div>
                    </div> --}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-close" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-save">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
