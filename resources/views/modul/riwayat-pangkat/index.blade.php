@extends('layout.template')

@section('title')
    Riwayat Kepangkatan
@endsection
@section('content')
<div class="mainpanel">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <ul class="breadcrumb">
                    <li><a href="{{ url('/home') }}"><i class="glyphicon glyphicon-home"></i></a></li>
                    <li>Data Riwayat Kepangkatan</li>
                </ul>
                <h4>Data Riwayat Kepangkatan</h4>
            </div>
        </div><!-- media -->
    </div><!-- pageheader -->

    <div class="contentpanel">
        <div class="panel panel-primary-head">
            <div class="panel-heading">
                <h4 class="panel-title">Data Riwayat Kepangkatan</h4>
                <br>
                <h6>Filter TMT Selanjutnya</h6>
                <form class="form-inline">
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputEmail2">Bulan</label>
                        <select name="bulan" id="bulan" class="form-control col-md-4">
                            <option value="">Pilih Bulan</option>
                            <option value="01">Januari</option>
                            <option value="02">Februari</option>
                            <option value="03">Maret</option>
                            <option value="04">April</option>
                            <option value="05">Mei</option>
                            <option value="06">Juni</option>
                            <option value="07">Juli</option>
                            <option value="08">Agustur</option>
                            <option value="09">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>
                        </select>
                    </div><!-- form-group -->

                    <div class="form-group">
                        <label class="sr-only" for="exampleInputPassword2">Tahun</label>
                        <select name="tahun" id="tahun" class="form-control">
                            <option value="">Pilih Tahun</option>
                            {{ loopingYears() }}
                        </select>
                    </div><!-- form-group -->
                </form>
            </div><!-- panel-heading -->
            <br>
            <button class="btn btn-sm btn-primary" onclick="tambahData()"><span class="fa fa-plus"></span> Tambah</button>
            <table id="riwayat_pangkat" class="table table-striped table-bordered responsive">
                <thead class="">
                    <tr>
                        <th>Nomor</th>
                        <th>Nama Lengkap</th>
                        <th>Pangkat</th>
                        <th>T.M.T</th>
                        <th>T.M.T Selanjutnya</th>
                        <th>Keterangan</th>
                        <th width="20%">Action</th>
                    </tr>
                </thead>
            </table>
        </div><!-- panel -->
    </div><!-- contentpanel -->
</div><!-- mainpanel -->
@include('modul.riwayat-pangkat.modal')
@endsection
@push('js')
<script>
    var riwayat_pangkat, save_method, bulan, tahun;
    $('#bulan').on('change', function(){
        bulan = $('#bulan').val();
        getRiwayatPangkat(bulan, tahun);
    });
    $('#tahun').on('change', function(){
        tahun = $('#tahun').val();
        getRiwayatPangkat(bulan, tahun);
    })
    function getRiwayatPangkat(bulan, tahun){
        riwayat_pangkat = $('#riwayat_pangkat').DataTable({
            ajax: {
                url : '{{ url("data/getDataRiwayat") }}',
                data: {
                    bulanResult : bulan,
                    tahunResult : tahun,
                },
            },
            processing: true,
            serverside: true,
            bDestroy : true,
            bLengthChange: false,
            scrollXInner: true,
            dom: 'frtp'
        });
    }
    $(function(){
        getRiwayatPangkat(null, null);
    })
    $('#kode_user, #pangkat_id, #bulan, #tahun').select2();
    function tambahData(){
        save_method = 'add';
        $('#title_riwayat').text('Tambah Riwayat');
        $('#modal_riwayat form')[0].reset();
        $('.btn-save').removeAttr('disabled','disabled');
        $('.btn-save').text('Save');
        $('#modal_riwayat').modal('show');
    }

    function updateData(id){
        save_method = 'edit';
        $('#title_riwayat').text('Update Riwayat');
        $('.btn_save').removeAttr('disabled','disabled');
        $('.btn0save').text('Update');
        $('#id').val(id);
        $.ajax({
            url : '{{ url("/riwayat-pangkat/getRiwayat") }}/'+id,
            type : "GET",
            success : function(data){
                $('#kode_user').val(data.kode_user).trigger('change');
                $('#pangkat_id').val(data.pangkat_id).trigger('change');
                $('#tmt').val(data.tmt);
                $('#keterangan').val(data.keterangan);

                $('#modal_riwayat').modal('show');
            },
            error: function(){
                Swal.fire(
                    'Error',
                    'Gagal mengambil data',
                    'error',
                )
            }
        })
    }

    $('.form-horizontal').on('submit', function(e){
        e.preventDefault();
        $('.btn-save').text('Saving . . .');
        $('.btn-save').attr('disabled');
        var id = $('#id').val();
        if(save_method == 'add'){
            url = '{{ url("/riwayat-pangkat/save") }}';
            message = 'Added';
        }else{
            url = '{{ url("/riwayat-pangkat/prosesUpdate") }}/'+id;
            message = 'Updated';
        }

        $.ajax({
            url : url,
            type : 'POST',
            data : $('.form-horizontal').serialize(),
            success: function(){
                $('#modal_riwayat').modal('hide');
                riwayat_pangkat.ajax.reload();
                Swal.fire(
                    message,
                    'Your data has been '+message,
                    'success',
                )
            },
            error: function(){
                Swal.fire(
                    'Error',
                    'Gagal menyimpan data',
                    'error',
                )
            }
        });
    });
</script>
@endpush
