<!DOCTYPE html>
<head>
    <title>Surat Pengajuan Cuti</title>
    <meta charset="utf-8">
    <style>
        #judul{
            text-align:center;
        }

        #halaman{
            width: auto;
            height: auto;
            position: absolute;
            border: 1px solid;
            padding-top: 30px;
            padding-left: 30px;
            padding-right: 30px;
            padding-bottom: 80px;
        }
        .tabelcss{
            border-collapse: collapse;
            border-width: 1px;
            border-style: solid;
        }
        td{
            padding-left: 10px;
        }
        .checkmark {
            font-family: arial;
            -ms-transform: scaleX(-1) rotate(-35deg); /* IE 9 */
            -webkit-transform: scaleX(-1) rotate(-35deg); /* Chrome, Safari, Opera */
            transform: scaleX(-1) rotate(-35deg);
          }
    </style>

</head>

<body>
    <p style="">
        <div style="float: right">
            Ungaran, <?php echo tanggal(date('Y-m-d')) ?><br>
            <span>Kepada Yth,</span>
            <p>di Ungaran</p>
        </div>
    </p>
    <p style="clear:both; text-align: center;">
        FORMULIR PERMINTAAN DAN PEMBERIAN CUTI
    </p>
    <table border="1" class="tabelcss" width="100%">
        <tr class="tabelcss">
            <td colspan="4">I. DATA PEGAWAI</td>
        </tr>
        <tr style="padding-left: 10px">
            <td width="80px">Nama</td>
            <td>{{ $user->nama_lengkap }}</td>
            <td width="90px">NIP</td>
            <td>{{ $user->nip }}</td>
        </tr>
        <tr>
            <td>Jabatan</td>
            <td>{{ $jabatan }}</td>
            <td>Masa Kerja</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Unit Kerja</td>
            <td colspan="3">Jawa Tengah - Kantor Pertanahan Kabupaten Semarang</td>
        </tr>
    </table>
    <br>
    {{--  <table border="1" class="tabelcss" width="100%">
        <tr class="tabelcss">
            <td colspan="4">II. JENIS CUTI YANG DIAMBIL</td>
        </tr>
        <tr style="padding-left: 10px">
            <td width="80px">Nama</td>
            <td>di sini Nama</td>
            <td width="90px">NIP</td>
            <td>di sini NIP</td>
        </tr>
        <tr>
            <td>Jabatan</td>
            <td>Disini Jabatan</td>
            <td>Masa Kerja</td>
            <td>Disini Masa KERJA</td>
        </tr>
        <tr>
            <td>Unit Kerja</td>
            <td colspan="3">di sini unit kerja</td>
        </tr>
    </table>  --}}
    <table border="1" class="tabelcss" width="100%">
        <tr class="tabelcss">
            <td colspan="4">II. ALASAN CUTI</td>
        </tr>
        <tr style="padding-left: 10px">
            <td colspan="4">{{ $cuti->alasan_cuti }}</td>
        </tr>
    </table>
    <br>
    <table border="1" class="tabelcss" width="100%">
        <tr class="tabelcss">
            <td colspan="6">III. LAMANYA CUTI</td>
        </tr>
        <tr style="padding-left: 10px; text-align: center;">
            <td width="80px">Selama</td>
            <td>{{ $cuti->jumlah_hari_cuti }} Hari Kerja</td>
            <td width="90px">Mulai Tanggal</td>
            <td>{{ tanggal($cuti->tanggal_cuti) }}</td>
            <td>s.d.</td>
            <td>{{ tanggal($cuti->tanggal_selesai_cuti) }}</td>
        </tr>
    </table>
    <br>
    <table border="1" class="tabelcss" width="100%">
        <tr class="tabelcss">
            <td colspan="5">IV. ALAMAT SELAMA MENJALANKAN CUTI</td>
        </tr>
        <tr style="padding-left: 10px">
            <td colspan="3">{{ $cuti->alamat_cuti }}</td>
            <td width="90px;">Telp</td>
            <td>{{ $cuti->no_handphone }}</td>
        </tr>
        <tr>
            <td colspan="3" height="120px;"></td>
            <td colspan="2" style="text-align: center;">
                <div>
                    Hormat Saya,
                </div>
                <br><br><br>
                {{ $user->nama_lengkap }}
                <br>
                <span>{{ $user->nip }}</span>
            </td>
        </tr>
    </table>
    <br>
    <table width="100%" class="tabelcss" border="1">
        <tr class="tabelcss">
            <td colspan="4">VI. KEPUTUSAN PEJABAT YANG BERWENANG MEMBERIKAN CUTI</td>
        </tr>
        <tr style="padding-left: 10px; text-align: center;">
            <td>DISETUJUI</td>
            <td>PERUBAHAN****</td>
            <td>DITANGGUHKAN****</td>
            <td width="40%">TIDAK DISETUJUI</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
        </tr>
    </table>
    {{-- <table style="float: right; padding-right; 40px;" width="40%%">
        <tr>
            <td colspan="5" style="text-align: center;">
                Kepala Kantor Pertanahan
                <br>
                Kabupaten Semarang
                <br><br><br><br>
                Dr. Arya Widya Wasista., S.T, M.Si
                <br>
                NIP. 196511051986031001
            </td>
        </tr>
    </table> --}}
    <p style="clear:both; text-align: center;">
        <table width="100%">
            <tr>
                <td>Catatan</td>
            </tr>
            <tr>
                <td width="10%">*</td>
                <td>Coret yang tidak perlu</td>
            </tr>
            <tr>
                <td width="10%">**</td>
                <td>Pilih salah satu dengan memberi tanda centang (&)</td>
            </tr>
            <tr>
                <td>***</td>
                <td>
                    Di isi oleh pejabatyang menangani bidang kepegawaian sebelum PNS mengajukan cuti
                </td>
            </tr>
            <tr>
                <td>****</td>
                <td>Diberi tanda centang dan alasannya</td>
            </tr>
            <tr>
                <td>N</td>
                <td>
                   = Cuti tahun berjalan
                </td>
            </tr>
            <tr>
                <td>N1</td>
                <td>= Sisa cuti 1 tahun sebelumnya</td>
            </tr>
            <tr>
                <td>N1</td>
                <td>= Sisa cuti 2 tahun sebelumnya</td>
            </tr>
        </table>
    </p>
</body>
</html>
