<div class="modal fade" id="modal_pengajuan_cuti" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <form class="form-horizontal">
                @csrf {{ method_field('POST') }}
                <div class="modal-header">
                    <h5 class="modal-title" id="title_pengajuan_cuti"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" value="">
                    <div class="form-group">
                        <div class="row">
                            <label for="module_name" class="col-md-2 control-label">Tanggal Mulai Cuti <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <input  type="text" id="tanggal_mulai_cuti" class="form-control" name="tanggal_mulai_cuti" required>
                                <span class="help-block with-errors"></span>
                             </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="module_name" class="col-md-2 control-label">Jumlah Hari <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <input type="number" min="0" class="form-control" id="jumlah_hari" name="jumlah_hari">
                                <span class="help-block with-errors"></span>
                             </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-2 control-label">No Handphone <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <input type="number" min="0" name="no_handphone" class="form-control" id="no_handphone" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-2 control-label">Alasan Cuti <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <input type="text" required class="form-control" name="alasan_cuti" id="alasan_cuti">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-2 control-label">Keterangan</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="keterangan" id="keterangan">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-2 control-label">Alamat Selama Cuti <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <textarea name="alamat_cuti" id="alamat_cuti" cols="82" rows="6" required></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-close" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-save">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="detail_pengajuan_cuti" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <form class="form-horizontal">
                <div class="modal-header">
                    <h5 class="modal-title" id="title_detail"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="module_name" class="col-md-2 control-label">Tanggal Mulai Cuti <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <strong id="tanggal_mulai_detail"></strong>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="module_name" class="col-md-2 control-label">Tanggal Berakhir Cuti <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <strong id="tanggal_berakhir_cuti_detail"></strong>
                             </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-2 control-label">Alamat Cuti <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <strong id="alamat_cuti_detail"></strong>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-2 control-label">Jumlah Hari <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <strong id="jumlah_hari_detail"></strong>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-2 control-label">Alasan Cuti <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <strong id="alasan_cuti_detail"></strong>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-2 control-label">No HandPhone yang bisa dihubungi <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <strong id="no_handphone_detail"></strong>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
