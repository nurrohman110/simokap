@extends('layout.template')

@section('title')
    Pengajuan Cuti
@endsection

@section('content')
<div class="mainpanel">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <ul class="breadcrumb">
                    <li><a href="{{ url('/home') }}"><i class="glyphicon glyphicon-home"></i></a></li>
                    <li>Data Pengajuan Cuti Pegawai</li>
                </ul>
                <h4>Data Pengajuan Cuti Pegawai</h4>
            </div>
        </div><!-- media -->
    </div><!-- pageheader -->

    <div class="contentpanel">
        <div class="panel panel-primary-head">
            <div class="panel-heading">
                <h4 class="panel-title">Data Pengajuan Cuti Pegawai</h4>
            </div><!-- panel-heading -->
            <br>
            <button class="btn btn-sm btn-primary" onclick="tambahData()"><span class="fa fa-plus"></span> Tambah</button>
            <table id="pengajuan_cuti" class="table table-striped table-bordered responsive">
                <thead class="">
                    <tr>
                        <th>Nomor</th>
                        {{-- <th>Nama Lengkap</th> --}}
                        <th>Tanggal Mulai Cuti</th>
                        <th>Tanggal Berakhir Cuti</th>
                        <th>Alamat Cuti</th>
                        <th>Jumlah hari</th>
                        <th>Sisa Cuti</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div><!-- panel -->
    </div><!-- contentpanel -->
</div><!-- mainpanel -->
@include('modul.pengajuan-cuti.modal');
@endsection

@push('js')
    <script>
        var tabel_pengajuan_cuti, save_method;
        $(function(){
            tabel_pengajuan_cuti = $('#pengajuan_cuti').DataTable({
                ajax: {
                    url : '{{ url("/data/pengajuan_cuti") }}',
                },
                processing: true,
                serverside: true,
                bDestroy : true,
                LengthChange: false,
                scrollXInner: true,
                dom: 'lBfrtip',
            });
        });

        function detailPengajuanCuti(kode_cuti){
            $('#title_detail').text('Detail Cuti');
            $.ajax({
                url : '{{ url("pengajuan-cuti/detail") }}/'+kode_cuti,
                type : 'GET',
                success : function(data){
                    $('#tanggal_mulai_detail').text(data.tanggal_cuti);
                    $("#tanggal_berakhir_cuti_detail").text(data.tanggal_selesai_cuti);
                    $('#alamat_cuti_detail').text(data.alamat_cuti_detail);
                    $('#jumlah_hari_detail').text(data.jumlah_hari_cuti+' hari');
                    $('#alasan_cuti_detail').text(data.alasan_cuti);
                    $('#no_handphone_detail').text(data.no_handphone);
                    $('#detail_pengajuan_cuti').modal('show');
                },
                error: function(){
                    Swal.fire({
                        icon: 'error',
                        title: 'Gagal mengambil data',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
            })
        }

        function tambahData(){
            save_method = 'add';
            $('.form-horizontal')[0].reset();
            $('#title_pengajuan_cuti').text('Pengajuan Cuti');
            $('.btn-save').prop("disabled", false);
            $('.btn-save').addClass('btn btn-primary');
            $('.btn-save').text('Save');
            $('#modal_pengajuan_cuti').modal('show');
        }
        $('.form-horizontal').on('submit', function(e){
            e.preventDefault();
            $('.btn-save').attr('disabled','');
            $('.btn-save').text('Saving . .. .');
            if(save_method == 'add'){
                url = '{{ url("pengajuan-cuti/save_pengajuan") }}';
                msg = 'Success';
            }
            var jumlah = $('#jumlah_hari').val();

            $.ajax({
                url : url,
                type: 'POST',
                data : $('.form-horizontal').serialize(),
                success: function(data){
                    if(data.value == 4){
                        Swal.fire(
                            'Info',
                            'Anda belum mendapatkan cuti',
                            'info'
                        )
                        $('.btn-save').text('Save');
                        $('.btn-save').prop("disabled", false);
                    }else if(data.value == 1){
                        Swal.fire(
                            'Info',
                            'Sisa cuti anda tidak mencukupi untuk mengambil cuti '+jumlah+' hari',
                            'info',
                        )
                        $('.btn-save').text('Save');
                        $('.btn-save').prop("disabled", false);
                    }else if(data.value == 2){
                        Swal.fire(
                            'Info',
                            'Sisa cuti anda sudah habis',
                            'info'
                        )
                        $('.btn-save').text('Save');
                        $('.btn-save').prop("disabled", false);
                    }else if(data.value == 3){
                        $('#modal_pengajuan_cuti').modal('hide');
                        tabel_pengajuan_cuti.ajax.reload();
                        Swal.fire(
                            msg,
                            'Cuti berhasil di simpan',
                            'success',
                        )
                    }
                },
                error: function(){
                    Swal.fire(
                        'Error',
                        'Gagal menyimpan data',
                        'error',
                    )
                }
            })
        })
        $("#tanggal_mulai_cuti").datepicker({
            minDate: new Date(),
            dateFormat: 'yy-mm-dd'
        });
    </script>
@endpush
