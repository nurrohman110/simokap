@extends('layout.template')

@section('title')
    Activity Harian
@endsection

@section('content')
<div class="mainpanel">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <ul class="breadcrumb">
                    <li><a href="{{ url('/home') }}"><i class="glyphicon glyphicon-home"></i></a></li>
                    <li>Data Activity Harian</li>
                </ul>
                <h4>Data Activity Harian</h4>
            </div>
        </div><!-- media -->
    </div><!-- pageheader -->

    <div class="contentpanel">
        <div class="panel panel-primary-head">
            <a href="{{ url('activity-harian/tambah') }}" class="btn btn-sm btn-primary"><span class="fa fa-plus"></span> Tambah</a>
            <button class="btn btn-sm btn-info" onclick="cetakActivity()"><span class="fa fa-print"></span> Cetak</button>
            <br> <br>
            <h4>Filter</h4>
            <div class="row clearfix">
                <div class="col-md-3">
                    <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                        <i class="fa fa-calendar"></i>&nbsp;
                        <span></span> <i class="fa fa-caret-down"></i>
                    </div>
                </div>
                @if (Auth::user()->id_level != 3)
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <select name="select_pegawai" id="select_pegawai" class="forn-control">
                                <option value="">Pilih Nama Pegawai</option>
                                @foreach ($pegawai as $p)
                                    <option value="{{ $p->kode_user }}">{{ $p->nama_lengkap }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <div class="table-responsive">
                <table id="activity_harian" class="table table-striped table-bordered responsive">
                    <thead class="">
                        <tr>
                            <th>Nomor</th>
                            <th>Tanggal</th>
                            <th>Uraian Tugas</th>
                            <th width="20%">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div><!-- panel -->
    </div><!-- contentpanel -->
</div><!-- mainpanel -->
@include('modul.activity-harian.modal');
@endsection

@push('js')
    <script>
        var tabel_activity, save_method, rangeAwal, rangeSelesai;
        let thisMoment = moment();
        var start = moment(thisMoment).startOf('month');
        var end = moment(thisMoment).endOf('month');
        $('#select_pegawai').select2();
        $(function(){
            $('#tanggal').datepicker({
                defaultDate: -1,
                minDate: -1,
                dateFormat: 'yy-mm-dd'
            });
        });

        function detailActivity(id){
            $('#title_detail').text('Detail Activity');
            $.ajax({
                url : '{{ url("activity-harian/detailActivity") }}/'+id,
                type: 'GET',
                success: function(data){
                    console.log(data);
                    $('#tanggal_activity').text(data.data[0].tanggal);
                    $('#aktifitas').html(data.data[0].description);

                    $('#detail_activity').modal('show');
                },
                error: function(){
                    alert('Gagal mengambil data');
                }
            })
        }
        function tambahData(){
            save_method = 'add';
            $('#title_modal_activity').text('Activity Harian');
            $('.btn-save').text('Save');
            $('.btn-save').removeAttr('disabled');
            $('#modal_activity form').trigger('reset');
            $('#modal_activity').modal('show');
        }

        function cetakActivity(){
            var kode_user = '{{ encrypt(Auth::user()->kode_user) }}';
            var params = { kode_user: kode_user, mulai: rangeAwal, selesai: rangeSelesai};
            var str = jQuery.param( params );

            var url = '{{ url("/activity-harian/cetak_activity") }}?'+str;
            window.open(url);
        }

        $('.form-horizontal').on('submit', function(e){
            e.preventDefault();
            $('.btn-save').text('Save');
            $('.btn-save').attr('disabled');
            if(save_method == 'add'){
                url = '{{ url("/activity-harian/save_activity") }}';
                message = 'Tambahkan';
            }else{

            }
            $.ajax({
                url : url,
                type : 'POST',
                cache : false,
                async : true,
                global : false,
                data : $('.form-horizontal').serialize(),
                success : function(data){
                    $('#modal_activity').modal('hide');
                    tabel_activity.ajax.reload();
                    Swal.fire({
                        icon: 'success',
                        title: 'Data berhasil di'+message,
                        showConfirmButton: false,
                        timer: 1500
                    })
                },
                error : function(){
                    $('#moda_activity').modal('hide');
                    Swal.fire({
                        icon: 'error',
                        title: 'Data gagal di'+message,
                        showConfirmButton: false,
                        timer: 1500
                    })
                },
            });
        });

        $('#select_pegawai').on('change', function(){
            var kode_user = $(this).val();
            console.log(rangeAwal, rangeSelesai);
            get_activity(rangeAwal, rangeSelesai, kode_user);
        })
        function get_activity(startDate, endDate, kode_user){
            tabel_activity = $('#activity_harian').DataTable({
                ajax: {
                    url : '{{ url("/data/activity_harian") }}',
                    data : {
                        mulai : rangeAwal,
                        selesai : rangeSelesai,
                        kode_user : kode_user,
                    }
                },
                processing: true,
                serverside: true,
                bDestroy : true,
                LengthChange: false,
                scrollXInner: true,
                paging: true,
                dom: 'lBfrtip',
            });
        }
        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            rangeAwal = start.format('YYYY-MM-DD');
            rangeSelesai =  end.format('YYYY-MM-DD');
            get_activity(rangeAwal, rangeSelesai, '');

        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);
        cb(start, end);
    </script>
@endpush
