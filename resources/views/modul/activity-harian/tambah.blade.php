@extends('layout.template')

@section('title')
    Tambah
@endsection
@section('content')
<div class="mainpanel">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <ul class="breadcrumb">
                    <li><a href="{{ url('/home') }}"><i class="glyphicon glyphicon-home"></i></a></li>
                    <li>Tambah data aktifitas harian</li>
                </ul>
                <h4>Tambah data aktifitas harian</h4>
            </div>
        </div><!-- media -->
    </div><!-- pageheader -->

    <div class="contentpanel">
        <div class="row">
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-btns">
                            <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                            <a href="" class="panel-close tooltips" data-toggle="tooltip" title="Close Panel"><i class="fa fa-times"></i></a>
                        </div><!-- panel-btns -->
                        <h4 class="panel-title">Data Aktifitas Harian</h4>
                        <p>Silahkan mengisi form dibawah ini, setiap hari</p>
                    </div><!-- panel-heading -->

                    <div class="panel-body nopadding">
                        <form class="form-horizontal form-bordered" action='{{ url("/activity-harian/save_activity") }}' method="POST">
                            @csrf
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tanggal</label>
                                <div class="col-sm-8">
                                    <input placeholder="Masukkan Tanggal kegiatan" id="tanggal" name="tanggal" class="form-control" required />
                                </div>
                            </div>
                            <div class="form-group fieldGroup">
                                <div class="row">
                                    <label for="" class="col-sm-4 control-label">Activity</label>
                                    <div class="col-md-8">
                                        <input type="text" name="activity[]" id="activity" class="form-control" placeholder="Masukkan Kegiatan Aktifitas Anda"/>
                                        <div class="input-group-addon ml-3">
                                            <a href="javascript:void(0)" class="btn btn-success addMore"><i class="fa fa-plus"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group fieldGroupCopy" style="display: none;">
                                <div class="row">
                                    <label for="" class="col-sm-4 control-label"></label>
                                    <div class="col-md-8">
                                        <input type="text" name="activity[]" class="form-control" placeholder="Masukkan Kegiatan Aktifitas Anda"/>
                                        <div class="input-group-addon">
                                            <a href="javascript:void(0)" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <button class="btn btn-primary mr5">Submit</button>
                                <button type="reset" class="btn btn-default">Reset</button>
                            </div><!-- panel-footer -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script>
    $(document).ready(function(){
        $('#tanggal').datepicker({
            defaultDate: -1,
            minDate: -1,
            dateFormat: 'yy-mm-dd'
        });
        var maxGroup = 10;
        $(".addMore").click(function(){
            if($('body').find('.fieldGroup').length < maxGroup){
                var fieldHTML = '<div class="form-group fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
                $('body').find('.fieldGroup:last').after(fieldHTML);
            }else{
                alert('Maximum '+maxGroup+' groups are allowed.');
            }
        });

        //remove fields group
        $("body").on("click",".remove",function(){
            $(this).parents(".fieldGroup").remove();
        });
    })
</script>
@endpush
