<div class="modal fade" id="modal_activity" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <form class="form-horizontal">
                @csrf {{ method_field('POST') }}
                <div class="modal-header">
                    <h5 class="modal-title" id="title_modal_activity"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-2 control-label">Tanggal</label>
                            <div class="col-md-8">
                                <input type="data" class="form-control" required name="tanggal" id="tanggal">
                            </div>
                        </div>
                    </div>
                    {{--  <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-2 control-label">Activity</label>
                            <div class="col-md-8">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                      <textarea id="ckeditor" placeholder="Enter text here..." class="form-control" name="cktext" required rows="10"></textarea>
                                        <div id="text_area" class="form-control" contentEditable="true">
                                            <ol>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  --}}
                    <div class="form-group fieldGroup">
                        <div class="row">
                            <label for="" class="col-md-2 control-label">Activity</label>
                            <div class="col-md-8">
                                <input type="text" name="activity[]" id="activity" class="form-control" placeholder="Masukkan Kegiatan Aktifitas Anda"/>
                                <div class="input-group-addon ml-3">
                                    <a href="javascript:void(0)" class="btn btn-success addMore"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group fieldGroupCopy" style="display: none;">
                        <div class="row">
                            <label for="" class="col-md-2  control-label"></label>
                            <div class="col-md-8">
                                <input type="text" name="activity[]" class="form-control" placeholder="Masukkan Kegiatan Aktifitas Anda"/>
                                <div class="input-group-addon">
                                    <a href="javascript:void(0)" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-close" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-save">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="detail_pegawai" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <form class="form-horizontal">
                @csrf {{ method_field('POST') }}
                <div class="modal-header">
                    <h5 class="modal-title" id="title_detail_pegawai"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        {{--  <div class="col-md-4">
                            <strong>Foto</strong>
                        </div>  --}}
                        <div class="col-md-8">
                            <div class="form-gruop">
                                <div class="row">
                                    <label for="" class="col-md-3">Nama Lengkap</label>
                                    <div class="col-md-5">
                                        <strong id="nama_lengkap_detail"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="form-gruop">
                                <div class="row">
                                    <label for="" class="col-md-3">NIP</label>
                                    <div class="col-md-5">
                                        <strong id="nip_detail"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="form-gruop">
                                <div class="row">
                                    <label for="" class="col-md-3">Password</label>
                                    <div class="col-md-5">
                                        <strong id="password_detail"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="form-gruop">
                                <div class="row">
                                    <label for="" class="col-md-3">Role</label>
                                    <div class="col-md-5">
                                        <strong id="role_detail"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="form-gruop">
                                <div class="row">
                                    <label for="" class="col-md-3">Tempat, Tanggal Lahir</label>
                                    <div class="col-md-5">
                                        <strong id="ttl_detail"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="form-gruop">
                                <div class="row">
                                    <label for="" class="col-md-3">Jenis Kelamin</label>
                                    <div class="col-md-5">
                                        <strong id="jenis_kelamin_detail"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="form-gruop">
                                <div class="row">
                                    <label for="" class="col-md-3">Agama</label>
                                    <div class="col-md-5">
                                        <strong id="agama_detail"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="form-gruop">
                                <div class="row">
                                    <label for="" class="col-md-3">Nomor Telepon</label>
                                    <div class="col-md-5">
                                        <strong id="nomor_telepon_detail"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="form-gruop">
                                <div class="row">
                                    <label for="" class="col-md-3">Status Pegawai</label>
                                    <div class="col-md-5">
                                        <strong id="status_pegawai_detail"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="form-gruop">
                                <div class="row">
                                    <label for="" class="col-md-3">Alamat</label>
                                    <div class="col-md-5">
                                        <strong id="alamat_detail"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="form-gruop">
                                <div class="row">
                                    <label for="" class="col-md-3">NPWP</label>
                                    <div class="col-md-5">
                                        <strong id="npwp_detail"></strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="detail_activity" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <form class="form-horizontal">
                <div class="modal-header">
                    <h5 class="modal-title" id="title_detail"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="module_name" class="col-md-2 control-label">Tanggal <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <strong id="tanggal_activity"></strong>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="module_name" class="col-md-2 control-label">Kegiatan Harian <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <strong id="aktifitas"></strong>
                             </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<div id="loading" class="text-center">
    <div class="spinner-border" role="status">
        <span class="sr-only">Loading...</span>
    </div>
</div>
