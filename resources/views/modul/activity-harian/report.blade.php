<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
        <title>{{ $nama }} | Laporan Activity</title>
        <style>
            body {
                font-family: "Arial Narrow", Arial, Helvetica, sans-serif;
                padding-top: 40px;
                padding-bottom: 70px;
            }

            .header, .footer {
                display: block;
            }
            .header {
                width: 100%;
                position: fixed;
                top: -30px;
            }
            .footer {
                width: 100%;
                position: fixed;
                bottom: 75px;
            }

            .content {
                margin-bottom: 5px;
                width: 100%;
            }

            .title {
                font-family: Arial, Helvetica, sans-serif;
                text-align: center;
                text-transform: uppercase;
            }

            .subtitle {
                text-align: center;
                font-size: 13px;
                font-weight: bold;
            }

            hr {
                display: block;
                margin-before: 0.5em;
                margin-after: 0.5em;
                margin-start: auto;
                margin-end: auto;
                overflow: hidden;
                border-style: inset;
                border-width: 1px;
                border-color: #eee;
            }

            .tabel_pdf {
                font-family: "Arial Narrow", Arial, Helvetica, sans-serif;
                font-size: 11px;
                width: 100%;
                border: 1px;
                margin-top: 30px;
                color: white;
                border:1px #463D75 solid;
            }

            .tabel_pdf thead {
                background-color: #463D75;
                font-weight: lighter;
                text-align: center;
            }

            .tabel_pdf tbody {
                color: black;
            }

            tr:nth-child(even) {
                background-color: #f2f2f2
            }

            .info {
                font-size: 11px;
                width: 100%;
            }

            .info tr:nth-child(even) {
                background-color: #fff;
            }

            .info th {
                font-weight: normal;
            }

            #container{
                width:100%;
                font-size: 11px;
                text-transform: uppercase;
                font-weight: bold;
            }
            #left{
                float:left;
            }
            #right{
                float:right;
            }
            #center{
                margin:0 auto;
                width:240px;
            }
        </style>
    </head>
    <body>
        {{--  <div class="header">
            <strong>Logo Header</strong>
        </div>
        <div class="footer">
            <strong>Logo Footer</strong>
        </div>  --}}
        <div class="content">
            <div class="title">
                <b>Laporan Activity Harian</b> <br>
                <b style="font-size: 15px; ">Tanggal Cetak : {{ tanggal(date('Y-m-d')) }} {{ date('h:i:s') }} WIB</b>
            </div>
            <hr width="100%"></hr>
            <div id="container">
                <div id="left">
                    <table width="35%">
                        <tr>
                            <td>Nama Lengkap</td>
                            <td>:</td>
                            <td>{{ $user->nama_lengkap }}</td>
                        </tr>
                        <tr>
                            <td>NIP</td>
                            <td>:</td>
                            <td>{{ $user->nip }}</td>
                        </tr>
                    </table>
                </div>
                <div id="right">
                    <table width="35%">
                        <tr>
                            <td>Tanggal Gabung</td>
                            <td>:</td>
                            <td>{{ $user->tanggal_gabung ?? '-' }}</td>
                        </tr>
                    </table>
                </div>
                <div id="center">
                    {{ $mulai }} s.d {{ $selesai }}
                </div>
            </div>
            <br>
            <br>
            <table class="tabel_pdf">
                <thead>
                    <tr>
                        <th width="90px;">No</th>
                        <th>Tanggal</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($activity as $a)
                        @if ($a['status_tanggal'])
                            <tr style="text-align: center; background-color: red; color: white;">
                                <td>{{ $a['nomor'] }}</td>
                                <td>{{ $a['tanggal'] }}</td>
                                <td style="text-align: left;">{!! $a['description'] !!}</td>
                            </tr>
                        @else
                            <tr style="text-align: center;">
                                <td>{{ $a['nomor'] }}</td>
                                <td>{{ $a['tanggal'] }}</td>
                                <td style="text-align: left;">{!! $a['description'] !!}</td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </body>
</html>

