<div class="modal fade" id="modal_kenaikan_gaji" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <form class="form-horizontal">
                @csrf {{ method_field('POST') }}
                <div class="modal-header">
                    <h5 class="modal-title" id="title_modal"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="hidden" id="id" value="">
                        <div class="row">
                            <label for="module_name" class="col-md-2 control-label">Pilih Nama <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <select name="nama_pegawai" id="pilih_nama" required class="form-control">
                                    <option value="">Pilih Nama</option>
                                    @foreach ($pegawai as $p)
                                        <option value="{{ $p->kode_user }}">{{ $p->nama_lengkap }}</option>
                                    @endforeach
                                </select>
                             </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="row">
                            <label for="module_name" class="col-md-2 control-label">Tanggal Terakhir Kenaikan Gaji <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <input type="date" class="form-control" id="tanggal" name="tanggal">
                                <span class="help-block with-errors"></span>
                             </div>
                        </div>
                     </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-close" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-save">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
