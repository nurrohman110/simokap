@extends('layout.template')

@section('title')
    Kenaikan Gaji
@endsection

@section('content')
<div class="mainpanel">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <ul class="breadcrumb">
                    <li><a href="{{ url('/home') }}"><i class="glyphicon glyphicon-home"></i></a></li>
                    <li>Data Jadwal Kenaikan Gaji</li>
                </ul>
                <h4>Data Jadwal Kenaikan Gaji</h4>
            </div>
        </div><!-- media -->
    </div><!-- pageheader -->

    <div class="contentpanel">
        <div class="panel panel-primary-head">
            <div class="panel-heading">
                <h4 class="panel-title">Data Jadwal Kenaikan Gaji</h4>
                <br>
                <div class="row clearfix">
                    @if (Auth::user()->id_level != 3)
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <select name="bulan" id="bulan" class="form-control col-md-2">
                                    <option value="">Pilih Bulan</option>
                                    <option value="01">Januari</option>
                                    <option value="02">Februari</option>
                                    <option value="03">Maret</option>
                                    <option value="04">April</option>
                                    <option value="05">Mei</option>
                                    <option value="06">Juni</option>
                                    <option value="07">Juli</option>
                                    <option value="08">Agustur</option>
                                    <option value="09">September</option>
                                    <option value="10">Oktober</option>
                                    <option value="11">November</option>
                                    <option value="12">Desember</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <select name="tahun" id="tahun" class="form-control">
                                    <option value="">Pilih Tahun</option>
                                    {{ loopingYears() }}
                                </select>
                            </div>
                            {{--  <div class="col-md-2">
                                <select name="select_pegawai" id="select_pegawai" class="form-control" >
                                    <option value="">Pilih Nama Pegawai</option>
                                    @foreach ($pegawai as $p)
                                        <option value="{{ $p->kode_user }}">{{ $p->nama_lengkap }}</option>
                                    @endforeach
                                </select>
                            </div>  --}}
                        </div>
                    </div>
                    @endif
                </div>
            </div><!-- panel-heading -->
            <br>
            <button class="btn btn-sm btn-primary" onclick="tambahData()"><span class="fa fa-plus"></span> Tambah</button>
            <table id="kenaikan_gaji" class="table table-striped table-bordered responsive">
                <thead class="">
                    <tr>
                        <th>Nomor</th>
                        <th>Nama Lengkap</th>
                        <th>Tanggal Tahun Kenaikan Gaji</th>
                        @if (Auth::user()->id_level == 1)
                            <th width="20%">Action</th>
                        @endif
                    </tr>
                </thead>
            </table>
        </div><!-- panel -->
    </div><!-- contentpanel -->
</div><!-- mainpanel -->

@include('modul.kenaikan-gaji.modal')
@endsection

@push('js')
<script>
    var kenaikan_gaji, save_method, kode_user, bulan, tahun;
    $(document).ready(function(){
        get_kenaikan_gaji();
    });
    $('#select_pegawai').on('change', function(){
        var kode_user = $(this).val();
        get_kenaikan_gaji(kode_user, bulan, tahun);
    });
    $('#bulan').on('change', function(){
        bulan = $('#bulan').val();
        console.log(bulan);
        get_kenaikan_gaji(kode_user,bulan, tahun);
    });
    $('#tahun').on('change', function(){
        console.log(tahun);
        tahun = $('#tahun').val();
        get_kenaikan_gaji(kode_user,bulan, tahun);
    })
    $('#pilih_nama, #select_pegawai,#bulan, #tahun').select2({width: '100%'});
    function get_kenaikan_gaji(kode_user, bulan, tahun){
        kenaikan_gaji = $('#kenaikan_gaji').DataTable({
            ajax:  {
                url: '{{ url("data/get_kenaikan_gaji") }}',
                data : {
                    kode_user : kode_user,
                    bulanResult : bulan,
                    tahunResult : tahun,
                }
            },
            processing: true,
            serverside: true,
            bDestroy : true,
            LengthChange: false,
            scrollXInner: true,
            dom: 'lBfrtip'
        });
    }

    function tambahData(){
        save_method = 'add';
        $('#title_modal').text('Tambah jadwal kenaikan gaji');
        $('#modal_kenaikan_gaji form')[0].reset();
        $('#pilih_nama').trigger('change');
        $('.btn-save').prop('disabled', false);
        $('.btn-save').text('Save');
        $('#modal_kenaikan_gaji').modal('show');
    }

    function updateSchedule(id){
        save_method = 'edit';
        $('#id').val(id);
        $('#title_modal').text('Update jadwal kenaikan gaji');
        $('.btn-save').prop('disabled', false);
        $('.btn-save').text('Update');

        $.ajax({
            url : '{{ url("kenaikan-gaji/get_kenaikan_gaji") }}/'+id,
            type: 'GET',
            success: function(data){
                $('#pilih_nama').val(data.kode_user).trigger('change');
                $('#tanggal').val(data.tanggal);

                $('#modal_kenaikan_gaji').modal('show');
            },
            error: function(){
                Swal.fire({
                    icon: 'error',
                    title: 'Gagal mengambil data',
                    showConfirmButton: false,
                    timer: 1500,
                })
            }
        })
    }

    $('.form-horizontal').on('submit', function(e){
        e.preventDefault();
        $('.btn-save').text('Saving . . .');
        $('.btn-save').prop('disabled',true);
        var id = $('#id').val();

        if(save_method == 'add'){
            url = '{{ url("kenaikan-gaji/save_data") }}';
            message = 'di tambahkan';
        }else{
            url = '{{ url("kenaikan-gaji/update_data") }}/'+id;
            message = 'di update';
        }

        $.ajax({
            url: url,
            type: 'POST',
            data : $('.form-horizontal').serialize(),
            success: function(){
                $('#modal_kenaikan_gaji').modal('hide');
                kenaikan_gaji.ajax.reload();
                Swal.fire({
                    icon: 'success',
                    title: 'Data berhasil '+message,
                    showConfirmButton: false,
                    timer: 1500,
                })
            },
            error: function(){
                Swal.fire({
                    icon: 'error',
                    title: 'Gagal '+message,
                    showConfirmButton: false,
                    timer: 1500,
                })
            }
        })
    })
</script>
@endpush
