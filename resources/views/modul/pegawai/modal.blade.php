<div class="modal fade" id="modal_pegawai" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <form class="form-horizontal">
                @csrf {{ method_field('POST') }}
                <div class="modal-header">
                    <h5 class="modal-title" id="title_pegawai"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <input type="hidden" name="id" id="id">
                            <label for="" class="col-md-2 control-label">Nama Lengkap <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="nama_lengkap" id="nama_lengkap" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-2 control-label">Nama Panggilan <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="nama_panggilan" required id="nama_panggilan">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-2 control-label">NIP <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <input type="number" min="0" class="form-control" required name="nip" id="nip">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-2 control-label">Level User <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <select name="level" class="form-control" id="level" required>
                                    <option value="">Pilih Level</option>
                                    @foreach ($level as $l)
                                        <option value="{{ $l->id }}">{{ $l->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-2 control-label">Jenis Kelamin <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <select name="jenis_kelamin" class="form-control" required id="jenis_kelamin">
                                    <option value="">Pilih Jenis Kelamin</option>
                                    <option value="L">Laki-Laki</option>
                                    <option value="P">Perempuan</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-2 control-label">Tempat Lahir <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-2 control-label">Tanggal Lahihr <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <input type="date" class="form-control" name="tanggal_lahir" id="tanggal_lahir" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-2 control-label">Agama <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <select name="agama" class="form-control" id="agama" required>
                                    <option value="">Pilih Agama</option>
                                    @foreach ($agama as $a)
                                        <option value="{{ $a->id }}">{{ $a->nama_agama }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-2 control-label">Type Pegawai <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <select name="type_pegawai" class="form-control" id="type_pegawai">
                                    <option value="">Pilih Jenis Pegawai</option>
                                    @foreach ($jenis as $i)
                                        <option value="{{ $i->id }}">{{ $i->nama_jenis }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-2 control-label">Pendidikan Terakhir</label>
                            <div class="col-md-8">
                                <select name="pendidikan_terakhir" class="form-control" id="pendidikan_terakhir">]
                                    <option value="">Pendidikan Terakhir</option>
                                    <option value="SD">SD</option>
                                    <option value="SLTP">SLTP Sederajat</option>
                                    <option value="SLTA">SLTA Sederjat</option>
                                    <option value="DIII">DIII</option>
                                    <option value="S1">S1</option>
                                    <option value="S2">S2</option>
                                    <option value="S3">S3</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-2 control-label">Nomor Telepon <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <input type="number" name="nomor_telepon" id="nomor_telepon" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-2 control-label">NPWP</label>
                            <div class="col-md-8">
                                <input type="number" name="npwp" class="form-control" id="npwp" >
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-2 control-label">Alamat <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <input type="text" name="alamat" id="alamat" required class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-close" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-save">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="detail_pegawai" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <form class="form-horizontal">
                @csrf {{ method_field('POST') }}
                <div class="modal-header">
                    <h5 class="modal-title" id="title_detail_pegawai"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        {{--  <div class="col-md-4">
                            <strong>Foto</strong>
                        </div>  --}}
                        <div class="col-md-8">
                            <div class="form-gruop">
                                <div class="row">
                                    <label for="" class="col-md-3">Nama Lengkap</label>
                                    <div class="col-md-5">
                                        <strong id="nama_lengkap_detail"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="form-gruop">
                                <div class="row">
                                    <label for="" class="col-md-3">NIP</label>
                                    <div class="col-md-5">
                                        <strong id="nip_detail"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="form-gruop">
                                <div class="row">
                                    <label for="" class="col-md-3">Password</label>
                                    <div class="col-md-5">
                                        <strong id="password_detail"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="form-gruop">
                                <div class="row">
                                    <label for="" class="col-md-3">Role</label>
                                    <div class="col-md-5">
                                        <strong id="role_detail"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="form-gruop">
                                <div class="row">
                                    <label for="" class="col-md-3">Tempat, Tanggal Lahir</label>
                                    <div class="col-md-5">
                                        <strong id="ttl_detail"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="form-gruop">
                                <div class="row">
                                    <label for="" class="col-md-3">Pendidikan Terakhir</label>
                                    <div class="col-md-5">
                                        <strong id="pendidikan_terakhir_detail"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="form-gruop">
                                <div class="row">
                                    <label for="" class="col-md-3">Jenis Kelamin</label>
                                    <div class="col-md-5">
                                        <strong id="jenis_kelamin_detail"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="form-gruop">
                                <div class="row">
                                    <label for="" class="col-md-3">Agama</label>
                                    <div class="col-md-5">
                                        <strong id="agama_detail"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="form-gruop">
                                <div class="row">
                                    <label for="" class="col-md-3">Nomor Telepon</label>
                                    <div class="col-md-5">
                                        <strong id="nomor_telepon_detail"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="form-gruop">
                                <div class="row">
                                    <label for="" class="col-md-3">Status Pegawai</label>
                                    <div class="col-md-5">
                                        <strong id="status_pegawai_detail"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="form-gruop">
                                <div class="row">
                                    <label for="" class="col-md-3">Alamat</label>
                                    <div class="col-md-5">
                                        <strong id="alamat_detail"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="form-gruop">
                                <div class="row">
                                    <label for="" class="col-md-3">NPWP</label>
                                    <div class="col-md-5">
                                        <strong id="npwp_detail"></strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="loading" class="text-center">
    <div class="spinner-border" role="status">
        <span class="sr-only">Loading...</span>
    </div>
</div>
