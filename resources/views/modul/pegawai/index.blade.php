@extends('layout.template')
@section('title')
    Data Pegawai
@endsection

@section('content')
<div class="mainpanel">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <ul class="breadcrumb">
                    <li><a href="{{ url('/home') }}"><i class="glyphicon glyphicon-home"></i></a></li>
                    <li>Data Pegawai</li>
                </ul>
                <h4>Data Pegawai</h4>
            </div>
        </div><!-- media -->
    </div><!-- pageheader -->

    <div class="contentpanel">
        <div class="panel panel-primary-head">
            <div class="panel-heading">
                <h4 class="panel-title">Data Pegawai</h4>
            </div><!-- panel-heading -->
            <br>
            <button class="btn btn-sm btn-primary" onclick="tambahData()"><span class="fa fa-plus"></span> Tambah</button>
            <table id="pegawai_tabel" class="table table-striped table-bordered responsive">
                <thead class="">
                    <tr>
                        <th>Nomor</th>
                        <th>Kode Pegawai</th>
                        <th>Nama Lengkap</th>
                        <th>Nama Panggilan</th>
                        <th>NIP</th>
                        <th>Level User</th>
                        <th>Jenis Kelamin</th>
                        <th>Tempat Tanggal Lahir</th>
                        <th>Nomor Telepon</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div><!-- panel -->
    </div><!-- contentpanel -->
</div><!-- mainpanel -->
@include('modul.pegawai.modal')
@endsection

@push('js')
    <script>
        var tabel_pegawai, save_method;
        $(function(){
            tabel_pegawai = $('#pegawai_tabel').DataTable({
                ajax: {
                    url : '{{ url("data/data_pegawai") }}',
                },
                processing: true,
                serverside: true,
                bDestroy : true,
                LengthChange: false,
                scrollXInner: true,
                dom: 'lBfrtip',
            });
        });

        function detailPegawai(id){
            $('#title_detail_pegawai').text('Detail');
            $.ajax({
                url : '{{ url("/pegawai/detail_pegawai") }}/'+id,
                type : 'GET',
                success : function(data){
                    $('#nama_lengkap_detail').text(data.nama_lengkap);
                    $('#nip_detail').text(data.nip);
                    $('#password_detail').text(data.secret);
                    $('#role_detail').text(data.role);
                    $('#ttl_detail').text(data.tempat_lahir+' '+data.tanggal_lahir);
                    $('#jenis_kelamin_detail').text(data.kelamin);
                    $('#agama_detail').text(data.agama_detail);
                    $('#nomor_telepon_detail').text(data.no_telepon ?? '-');
                    $('#status_pegawai_detail').text(data.status_pegawai);
                    $('#alamat_detail').text(data.alamat_baru ?? '-');
                    $('#npwp_detail').text(data.npwp ?? '-');
                    $('#pendidikan_terakhir_detail').text(data.pendidikan_terakhir ?? '-');
                    setTimeout(function(){
                        $('#detail_pegawai').modal('show');
                    });
                },
                error : function(){
                    Swal.fire(
                        'Error',
                        'Gagal mengambil data',
                        'error',
                    )
                }
            });
        }

        $('#jenis_kelamin, #agama, #type_pegawai, #level, #pendidikan_terakhir').select2();

        function tambahData(){
            save_method = 'add';
            $('#title_pegawai').text('Tambah pegawai');
            $('.btn-save').removeAttr('disabled');
            $('.btn-save').text('Save');
            $('#jenis_kelamin, #agama, #type_pegawai, #pendidikan_terakhir').val('').trigger('change');
            $('#modal_pegawai form')[0].reset();
            $('#modal_pegawai').modal('show');
        }

        function updatePegawai(id){
            save_method = 'edit';
            $('#title_pegawai').text('Update Pegawai');
            $('.btn-save').removeAttr('disabled');
            $('.btn-save').text('Update');
            $('#id').val(id);
            $.ajax({
                url : '{{ url("pegawai/detail_pegawai") }}/'+id,
                type : 'GET',
                success: function(data){
                    $('#nama_lengkap').val(data.nama_lengkap);
                    $('#nama_panggilan').val(data.nama_panggilan);
                    $('#nip').val(data.nip);
                    $('#level').val(data.id_level).trigger('change');
                    $('#jenis_kelamin').val(data.jenis_kelamin).trigger('change');
                    $('#tempat_lahir').val(data.tempat_lahir);
                    $('#tanggal_lahir').val(data.tanggal_lahir);
                    $('#agama').val(data.agama_id).trigger('change');
                    $('#type_pegawai').val(data.jenis_pegawai_id).trigger('change');
                    $('#nomor_telepon').val(data.no_telepon);
                    $('#npwp').val(data.npwp);
                    $('#alamat').val(data.alamat_baru);
                    $('#pendidikan_terakhir').val(data.pendidikan_terakhir).trigger('change');
                    $('#modal_pegawai').modal('show');
                },
                error : function(){
                    Swal.fire(
                        'Gagal',
                        'Gagal mengambil data',
                        'error',
                    )
                }
            });
        }

        function hapusPegawai(id){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                console.log(result.isConfirmed);
                if (result.isConfirmed) {
                    $.ajax({
                        url : '{{ url("/pegawai/delete_pegawai") }}/'+id,
                        type: 'POST',
                        data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
                        success: function(data){
                            Swal.fire(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                            ),
                            tabel_pegawai.ajax.reload();
                        },
                        error: function(){
                            Swal.fire(
                                'Cancelled',
                                'Gagal menghapus data :)',
                                'error'
                            )
                        }
                    });
                }else{
                    Swal.fire(
                        'Cancelled',
                        'Delete di cancel :)',
                        'error'
                    )
                }
            })
        }

        $('.form-horizontal').on('submit', function(e){
            e.preventDefault();
            $('.btn-save').attr('disabled');
            $('.btn-save').text('Saving . . .');
            var id = $('#id').val();
            if(save_method == 'add'){
                url = '{{ url("/pegawai/save") }}';
                msg = 'Added';
            }else{
                url = '{{ url("/pegawai/update_pegawai") }}/'+id;
                msg = 'Updated';
            }

            $.ajax({
                url : url,
                type : 'POST',
                data : $('.form-horizontal').serialize(),
                success: function(data){
                    $('#modal_pegawai').modal('hide');
                    tabel_pegawai.ajax.reload();
                    Swal.fire(
                        msg,
                        'Data berhasil di '+msg,
                        'success'
                    )
                },
                error : function(){
                    Swal.fire(
                        'Error',
                        'Data gagal di '+msg,
                        'error',
                    )
                }
            })
        });
    </script>
@endpush
