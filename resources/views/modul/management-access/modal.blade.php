<div class="modal fade" id="modal_management" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <form class="form-horizontal">
                @csrf {{ method_field('POST') }}
                <div class="modal-header">
                    <h5 class="modal-title" id="title_management"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="module_name" class="col-md-2 control-label">Level <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <input type="hidden" id="id" value="">
                                <select name="level" id="level" class="form-control" required>
                                    <option value="">Pilih Level</option>
                                    @foreach ($level as $l)
                                        <option value="{{ $l->id }}">{{ $l->name }}</option>
                                    @endforeach
                                </select>
                                <span class="help-block with-errors"></span>
                             </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="module_name" class="col-md-2 control-label">Nama Modul <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <select name="modul" class="form-control" id="modul" required>
                                    <option value="">Pilih Modul</option>
                                    @foreach ($modul as $m)
                                        <option value="{{ $m->id }}">{{ $m->nama_modul }}</option>
                                    @endforeach
                                </select>
                                <span class="help-block with-errors"></span>
                             </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-close" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-save">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
