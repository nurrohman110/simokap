@extends('layout.template')

@section('title')
    Management Access
@endsection

@section('content')
<div class="mainpanel">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <ul class="breadcrumb">
                    <li><a href="{{ url('/home') }}"><i class="glyphicon glyphicon-home"></i></a></li>
                    <li>Data Management Access</li>
                </ul>
                <h4>Data Management Access</h4>
            </div>
        </div><!-- media -->
    </div><!-- pageheader -->

    <div class="contentpanel">
        <div class="panel panel-primary-head">
            <div class="panel-heading">
                <h4 class="panel-title">Data Management Access</h4>
            </div><!-- panel-heading -->
            <br>
            <button class="btn btn-sm btn-primary" onclick="tambahData()"><span class="fa fa-plus"></span> Tambah</button>
            <table id="management_access" class="table table-striped table-bordered responsive">
                <thead class="">
                    <tr>
                        <th>Nomor</th>
                        <th>Level</th>
                        <th>Nama Modul</th>
                        <th width="20%">Action</th>
                    </tr>
                </thead>
            </table>
        </div><!-- panel -->
    </div><!-- contentpanel -->
</div><!-- mainpanel -->
@include('modul.management-access.modal')
@endsection

@push('js')
<script>
    var managementAccess, save_method;
    $('#level, #modul').select2();
    $(function () {
        managementAccess = $('#management_access').DataTable({
            ajax: '{{ url("data/management-access") }}',
            processing: true,
            serverside: true,
            bLengthChange: false,
            scrollXInner: true,
            dom: 'frtp'
        });
    });
    function tambahData(){
        save_method = 'add';
        $('#title_management').text('Add Management Access');
        $('#modal_management form')[0].reset();
        $('.btn-save').removeAttr('disabled','disabled');
        $('.btn-save').text('Save');
        $('#level, #modul').trigger('change');
        $('#modal_management').modal('show');
    }

    function deleteManagement(id){
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            console.log(result.isConfirmed);
            if (result.isConfirmed) {
                $.ajax({
                    url : '{{ url("/management-access/hapus") }}/'+id,
                    type: 'POST',
                    data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
                    success: function(data){
                        Swal.fire(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        ),
                        managementAccess.ajax.reload();
                    },
                    error: function(){
                        Swal.fire(
                            'Cancelled',
                            'Gagal menghapus data :)',
                            'error'
                        )
                    }
                });
            }else{
                Swal.fire(
                    'Cancelled',
                    'Delete di cancel :)',
                    'error'
                )
            }
        })
    }

    function updateManagement(id){
        save_method = 'edit';
        $('#title_management').text('Update Management Access');
        $('#modal_management form')[0].reset();
        $('.btn-save').removeAttr('disabled','disabled');
        $('.btn-save').text('Update');
        $('#id').val(id);
        $.ajax({
            url : '{{ url("management-access/getManagement") }}/'+id,
            type : 'GET',
            success: function(data){
                $('#level').val(data.id_level).trigger('change');
                $('#modul').val(data.id_modul).trigger('change');
                $('#modal_management').modal('show');
            },
            error: function(){
                Swal.fire(
                    'Error Input',
                    'Gagal mengambil data',
                    'error',
                )
            }
        })
    }

    $('.form-horizontal').on('submit', function(e){
        e.preventDefault();
        var id = $('#id').val();
        $('.btn-save').attr('disabled');
        $('.btn-save').text('Saving . . . .');
        var id = $('#id').val();
        if(save_method == 'add'){
            url = '{{ url("management-access/tambah") }}';
            message = 'Added';
        }else{
            url = '{{ url("management-access/update") }}/'+id;
            message = 'Update';
        }

        $.ajax({
            url : url,
            type: 'POST',
            data : $('.form-horizontal').serialize(),
            success: function(){
                $('#modal_management').modal('hide');
                managementAccess.ajax.reload();
                Swal.fire(
                    message,
                    'Your data has been '+message,
                    'success',
                )
            },
            error: function(){
                Swal.fire(
                    'Error Input',
                    'Gagal menyimpan data',
                    'error',
                )
            }
        })
    });
</script>
@endpush
