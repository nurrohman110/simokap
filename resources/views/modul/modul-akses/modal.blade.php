<div class="modal fade" id="modal_modul" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <form class="form-horizontal">
                @csrf {{ method_field('POST') }}
                <div class="modal-header">
                    <h5 class="modal-title" id="title_modul"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="module_name" class="col-md-2 control-label">Module Name <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <input type="hidden" id="module_id" value="">
                                <input  type="text" id="nama_modul" class="form-control" name="nama_modul" required>
                                <span class="help-block with-errors"></span>
                             </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="row">
                            <label for="module_name" class="col-md-2 control-label">URL Name <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="url_name" name="url_name" required>
                                <span class="help-block with-errors"></span>
                             </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="row">
                            <label for="module_name" class="col-md-2 control-label">Icon <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="icon" name="icon" required>
                                <span class="help-block with-errors"></span>
                             </div>
                        </div>
                     </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-close" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-save">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
