@extends('layout.template')

@section('title')
    Modul
@endsection

@section('content')
<div class="mainpanel">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <ul class="breadcrumb">
                    <li><a href="{{ url('/home') }}"><i class="glyphicon glyphicon-home"></i></a></li>
                    <li>Data Modul</li>
                </ul>
                <h4>Data Modul</h4>
            </div>
        </div><!-- media -->
    </div><!-- pageheader -->

    <div class="contentpanel">
        <div class="panel panel-primary-head">
            <div class="panel-heading">
                <h4 class="panel-title">Data Modul</h4>
            </div><!-- panel-heading -->
            <br>
            <button class="btn btn-sm btn-primary" onclick="tambahData()"><span class="fa fa-plus"></span> Tambah Modul</button>
            <table id="modul" class="table table-striped table-bordered responsive">
                <thead class="">
                    <tr>
                        <th>Nomor</th>
                        <th>Nama Modul</th>
                        <th>URL</th>
                        <th>Icon</th>
                        <th width="20%">Action</th>
                    </tr>
                </thead>
            </table>
        </div><!-- panel -->
    </div><!-- contentpanel -->
</div><!-- mainpanel -->
@include('modul.modul-akses.modal')
@endsection

@push('js')
<script>
    var module_table, save_method ;
    $(function () {
        module_table = $('#modul').DataTable({
            ajax: '{{ url("data/modul") }}',
            processing: true,
            serverside: true,
            bLengthChange: false,
            scrollXInner: true,
            dom: 'frtp'
        });
    });

    function tambahData(){
        save_method = 'add';
        $('#title_modul').text('Add Module');
        $('#modal_modul form')[0].reset();
        $('.btn-save').removeAttr('disabled','disabled');
        $('.btn-save').text('Save');
        $('#modal_modul').modal('show');
    }

    function updateModul(id){
        save_method = 'edit';
        $('#title_modul').text('Update Modul');
        $('.btn-save').removeAttr('disabled','disabled');
        $('.btn-save').text('Update');

        $.ajax({
            url : '{{ url("/modul/getModul") }}/'+id,
            type: 'GET',
            success: function(data){
                $('#module_id').val(id);
                $('#nama_modul').val(data.nama_modul);
                $('#url_name').val(data.url);
                $('#icon').val(data.icon);
                $('#modal_modul').modal('show');
            },
            error: function(){
                Swal.fire(
                    'Cancelled',
                    'Gagal mengambil data :)',
                    'error'
                )
            },
        })
    }

    function deleteModul(id){
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            console.log(result.isConfirmed);
            if (result.isConfirmed) {
                $.ajax({
                    url : '{{ url("/modul/hapus") }}/'+id,
                    type: 'POST',
                    data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
                    success: function(data){
                        Swal.fire(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        ),
                        module_table.ajax.reload();
                    },
                    error: function(){
                        Swal.fire(
                            'Cancelled',
                            'Gagal menghapus data :)',
                            'error'
                        )
                    }
                });
            }else{
                Swal.fire(
                    'Cancelled',
                    'Delete di cancel :)',
                    'error'
                )
            }
        })
    }

    $('.form-horizontal').on('submit', function(e){
        e.preventDefault();
        var id = $('#module_id').val();
        $('.btn-save').attr('disabled');
        $('.btn-save').text('Saving . . . .');
        if (save_method == 'add') {
            var url = '{{ url("/modul/save") }}',
            msg = 'added';
        } else {
            var url = '{{ url("/modul/update") }}/'+id,
            msg = 'updated';
        }
        $.ajax({
            url : url,
            type: 'POST',
            data : $('.form-horizontal').serialize(),
            success : function(){
                $('#modal_modul').modal('hide');
                module_table.ajax.reload();
                Swal.fire(
                    msg,
                    'Your data has been '+msg,
                    'success'
                )
            },
            error: function(){
                Swal.fire(
                    'Error Input',
                    'Gagal menyimpan data',
                    'error',
                )
            },
        });
    });
</script>
@endpush
