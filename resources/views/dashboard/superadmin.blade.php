@extends('layout.template')

@section('content')
<div class="mainpanel">
    <div class="pageheader">
        <div class="media">
            <div class="pageicon pull-left">
                <i class="fa fa-home"></i>
            </div>
            <div class="media-body">
                <ul class="breadcrumb">
                    <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                    <li>Dashboard</li>
                </ul>
                <h4>Dashboard</h4>
            </div>
        </div><!-- media -->
    </div><!-- pageheader -->

    <div class="contentpanel">
        <div class="row row-stat">
            <div class="alert alert-danger">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                <h4>WARNING!!</h4> Sudahkan anda mengisi aktifitas harian pada tanggal <strong>{{ $date_before }}</strong>
            </div>
            <div class="col-md-4">
                <div class="panel panel-success-alt noborder">
                    <div class="panel-heading noborder">
                        <div class="panel-btns">
                            <a href="" class="panel-close tooltips" data-toggle="tooltip" title="Close Panel"><i class="fa fa-times"></i></a>
                        </div><!-- panel-btns -->
                        <div class="panel-icon"><i class="fa fa-dollar"></i></div>
                        <div class="media-body">
                            <h5 class="md-title nomargin">Jadwal kenaikan gaji ada pada tanggal</h5>
                            <h1 class="mt5">{{ isset($gaji) ? $gaji->tanggal : '-' }}</h1>
                        </div><!-- media-body -->
                        <hr>

                    </div><!-- panel-body -->
                </div><!-- panel -->
            </div><!-- col-md-4 -->

            <div class="col-md-4">
                <div class="panel panel-primary noborder">
                    <div class="panel-heading noborder">
                        <div class="panel-btns">
                            <a href="" class="panel-close tooltips" data-toggle="tooltip" title="Close Panel"><i class="fa fa-times"></i></a>
                        </div><!-- panel-btns -->
                        <div class="panel-icon"><i class="fa fa-users"></i></div>
                        <div class="media-body">
                            <h5 class="md-title nomargin">Sisa cuti anda tahun ini</h5>
                            <h1 class="mt5">{{ isset($sisa_cuti) ? $sisa_cuti->sisa_cuti : "-" }} x</h1>
                        </div><!-- media-body -->
                        <hr>
                        {{-- <div class="clearfix mt20">
                            <div class="pull-left">
                                <h5 class="md-title nomargin">Yesterday</h5>
                                <h4 class="nomargin">10,009</h4>
                            </div>
                            <div class="pull-right">
                                <h5 class="md-title nomargin">This Week</h5>
                                <h4 class="nomargin">178,222</h4>
                            </div>
                        </div> --}}
                    </div><!-- panel-body -->
                </div><!-- panel -->
            </div><!-- col-md-4 -->
            <div class="col-md-4">
                <div class="panel panel-dark noborder">
                    <div class="panel-heading noborder">
                        <div class="panel-btns">
                            <a href="" class="panel-close tooltips" data-toggle="tooltip" data-placement="left" title="Close Panel"><i class="fa fa-times"></i></a>
                        </div><!-- panel-btns -->
                        <div class="panel-icon"><i class="fa fa-pencil"></i></div>
                        <div class="media-body">
                            <h5 class="md-title nomargin">Kenaikan Pangkat Selanjutnya Pada Tanggal</h5>
                            <h1 class="mt5">{{ isset($naik_pangkat) ? $naik_pangkat->tmt_selanjutnya : '' }}</h1>
                        </div><!-- media-body -->
                        <hr>
                    </div><!-- panel-body -->
                </div><!-- panel -->
            </div><!-- col-md-4 -->
        </div><!-- row -->
    </div><!-- contentpanel -->
</div><!-- mainpanel -->

@endsection
