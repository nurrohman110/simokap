@section('title')
    Home
@endsection

@role('SuperAdmin')
@include('dashboard.superadmin')
@endrole

@role('Admin')
@include('dashboard.superadmin')
@endrole

@role('User')
@include('dashboard.superadmin')
@endrole
